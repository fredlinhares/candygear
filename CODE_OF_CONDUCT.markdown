Benedictine Code of Conduct
===========================

Summum Bonum
------------

"Every art and every inquiry, and similarly every action and pursuit, is thought to aim at some good; and for this reason the good has rightly been declared to be that at which all things aim." - Aristotle of Stagira, Nicomachean Ethics

To ensure a healthy development environment and prevent insidious heretical indoctrination through free software projects, participation in the CandyGear is governed towards the highest good.

This project welcomes all Christians and all people that respect Christian values. Therefore, all participants are encouraged to follow the [Recommendations](#Recommendations) section, and everything under the [Rules](#Rules) section will be enforced.

Recommendations
---------------

01. First of all, love the Lord God with your whole heart, your whole soul, and your whole strength.
02. Then, love your neighbor as yourself.
03. Honor all people.
04. Deny oneself in order to follow Christ.
05. Chastise the body.
06. Do not become attached to pleasures.
07. Love fasting.
08. Relieve the poor.
09. Clothe the naked.
10. Visit the sick.
11. Bury the dead.
12. Be a stranger to the world's ways.
13. Prefer nothing more than the love of Christ.
14. Do not forsake charity.
15. Do not return evil for evil.
16. Do no wrong to anyone, and bear patiently wrongs done to yourself.
17. Love your enemies.
18. Do not curse those who curse you, but rather bless them.
19. Put your hope in God.
20. Attribute to God, and not to self, whatever good you see in yourself.
21. Recognize always that evil is your own doing, and to impute it to yourself.
22. Fear the Day of Judgment.
23. Be in dread of hell.
24. Desire eternal life with all the passion of the spirit.
25. Keep death daily before your eyes.
26. Keep constant guard over the actions of your life.
27. Know for certain that God sees you everywhere.
28. When wrongful thoughts come into your heart, dash them against Christ immediately.
29. Disclose wrongful thoughts to your spiritual mentor.
30. Listen willingly to holy reading.
31. Devote yourself frequently to prayer.
32. Daily in your prayers, with tears and sighs, confess your past sins to God, and amend them for the future.
33. Fulfill not the desires of the flesh; hate your own will.
34. Obey in all things the commands of those whom God has placed in authority over you even though they (which God forbid) should act otherwise, mindful of the Lord's precept, "Do what they say, but not what they do."
35. Do not wish to be called holy before one is holy; but first to be holy, that you may be truly so called.
36. Fulfill God's commandments daily in your deeds.
37. Love chastity.
38. Hate no one.
39. Pray for your enemies in the love of Christ.
40. Make peace with your adversary before the sun sets.
41. Never despair of God's mercy.

Rules
-----

01. Do not murder.
02. Do not commit adultery.
03. Do not steal.
04. Do not covet.
05. Do not bear false witness.
06. Do not do to another what you would not have done to yourself.
07. Be a help in times of trouble.
08. Console the sorrowing.
09. Do not give way to anger.
10. Do not nurse a grudge.
11. Do not entertain deceit in your heart.
12. Do not give a false peace.
13. Do not swear, for fear of perjuring yourself.
14. Utter only truth from heart and mouth.
15. Bear persecution for justice's sake.
16. Be not proud.
17. Be not addicted to wine.
18. Be not a great eater.
19. Be not drowsy.
20. Be not lazy.
21. Be not a grumbler.
22. Be not a detractor.
23. Guard your tongue against evil and depraved speech.
24. Do not love much talking.
25. Speak no useless words or words that move to laughter.
26. Do not love much or boisterous laughter.
27. Be not jealous, nor harbor envy.
28. Do not love quarreling.
29. Shun arrogance.
30. Respect your seniors.
31. Love your juniors.

Scope
-----

These rules apply within every official CandyGear community space and when an individual officially represents the project.

Enforcement
-----------

The moderators will take one of the following measurements to violations of the [Rules](#Rules):

- remove or replace any irregular content with one more appropriated;
- warnings when the violations are minor or mild;
- in case of grave infringement, a temporary banishment from 1 to 3 months;
- in case of grave infringement from a person that already received a banish, a banishment from 6 months to 2 years.

Reporting
---------

Any infringement of the [Rules](#Rules) can be reported at <fred@linhares.blue>. The moderators will promptly and thoroughly investigate all complaints.
