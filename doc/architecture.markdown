Architecture
============

This project is split into two parts BluCat and CandyGear.
BluCat is the C++ engine, and CandyGear is the mruby engine.
BluCat consists of a few modules:

- COM: common code that other modules use.
- INT: integration of all modules.
- GRA: graphics module.
