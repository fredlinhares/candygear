MRuby::Build.new do |conf|
  conf.toolchain

  conf.gem core: "mruby-struct"
  conf.gem core: "mruby-socket"
  conf.gembox "math"
  conf.gembox "metaprog"
  conf.gembox "stdlib"
  conf.gembox "stdlib-ext"

  conf.enable_bintest
  conf.enable_test
end
