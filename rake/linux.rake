# Copyright 2022-2024 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

OS_LIBRARIES = [
  'vulkan'
]

task install: [:build, :lib] do
  destdir = ENV['DESTDIR'] || ''

  # Install engine
  `install -d #{destdir}/usr/local/bin`
  `install #{OBJ} #{destdir}/usr/local/bin`

  # Install shaders
  `install -d #{destdir}#{DATA_DIR}/glsl`
  SPV_FILES.each {`install #{_1} #{destdir}#{DATA_DIR}/glsl`}
end
