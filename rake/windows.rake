# Copyright 2022-2024 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

OS_LIBRARIES = [
  'SDL2main',
  'mingw32',
  'vulkan-1.dll',
  'ws2_32'
]

task install: :build do
  environment = `echo $MSYSTEM`.downcase.chomp
  ENV['DLL_PATH'] = `cygpath -w /#{environment}/bin`.chomp
  ENV['CANDY_GEAR_VERSION'] = VERSION
  `makensis.exe windows_installer.nsi`
end
