/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "key.hpp"

void
cg_key_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_mKey;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_mKey = mrb_define_module_under(mrb, cg_m, "Key");

  mrb_define_const(mrb, cg_mKey, "A", mrb_int_value(mrb, SDLK_a));
  mrb_define_const(mrb, cg_mKey, "B", mrb_int_value(mrb, SDLK_b));
  mrb_define_const(mrb, cg_mKey, "C", mrb_int_value(mrb, SDLK_c));
  mrb_define_const(mrb, cg_mKey, "D", mrb_int_value(mrb, SDLK_d));
  mrb_define_const(mrb, cg_mKey, "E", mrb_int_value(mrb, SDLK_e));
  mrb_define_const(mrb, cg_mKey, "F", mrb_int_value(mrb, SDLK_f));
  mrb_define_const(mrb, cg_mKey, "G", mrb_int_value(mrb, SDLK_g));
  mrb_define_const(mrb, cg_mKey, "H", mrb_int_value(mrb, SDLK_h));
  mrb_define_const(mrb, cg_mKey, "I", mrb_int_value(mrb, SDLK_i));
  mrb_define_const(mrb, cg_mKey, "J", mrb_int_value(mrb, SDLK_j));
  mrb_define_const(mrb, cg_mKey, "K", mrb_int_value(mrb, SDLK_k));
  mrb_define_const(mrb, cg_mKey, "L", mrb_int_value(mrb, SDLK_l));
  mrb_define_const(mrb, cg_mKey, "M", mrb_int_value(mrb, SDLK_m));
  mrb_define_const(mrb, cg_mKey, "N", mrb_int_value(mrb, SDLK_n));
  mrb_define_const(mrb, cg_mKey, "O", mrb_int_value(mrb, SDLK_o));
  mrb_define_const(mrb, cg_mKey, "P", mrb_int_value(mrb, SDLK_p));
  mrb_define_const(mrb, cg_mKey, "Q", mrb_int_value(mrb, SDLK_q));
  mrb_define_const(mrb, cg_mKey, "R", mrb_int_value(mrb, SDLK_r));
  mrb_define_const(mrb, cg_mKey, "S", mrb_int_value(mrb, SDLK_s));
  mrb_define_const(mrb, cg_mKey, "T", mrb_int_value(mrb, SDLK_t));
  mrb_define_const(mrb, cg_mKey, "U", mrb_int_value(mrb, SDLK_u));
  mrb_define_const(mrb, cg_mKey, "V", mrb_int_value(mrb, SDLK_v));
  mrb_define_const(mrb, cg_mKey, "W", mrb_int_value(mrb, SDLK_w));
  mrb_define_const(mrb, cg_mKey, "X", mrb_int_value(mrb, SDLK_x));
  mrb_define_const(mrb, cg_mKey, "Y", mrb_int_value(mrb, SDLK_y));
  mrb_define_const(mrb, cg_mKey, "Z", mrb_int_value(mrb, SDLK_z));

  mrb_define_const(mrb, cg_mKey, "UP", mrb_int_value(mrb, SDLK_UP));
  mrb_define_const(mrb, cg_mKey, "DOWN", mrb_int_value(mrb, SDLK_DOWN));
  mrb_define_const(mrb, cg_mKey, "LEFT", mrb_int_value(mrb, SDLK_LEFT));
  mrb_define_const(mrb, cg_mKey, "RIGHT", mrb_int_value(mrb, SDLK_RIGHT));

  mrb_define_const(
    mrb, cg_mKey, "BACKSPACE", mrb_int_value(mrb, SDLK_BACKSPACE));
  mrb_define_const(mrb, cg_mKey, "TAB", mrb_int_value(mrb, SDLK_TAB));
  mrb_define_const(
    mrb, cg_mKey, "LEFT_SHIFT", mrb_int_value(mrb, SDLK_LSHIFT));
  mrb_define_const(
    mrb, cg_mKey, "RIGHT_SHIFT", mrb_int_value(mrb, SDLK_RSHIFT));
  mrb_define_const(mrb, cg_mKey, "SPACE", mrb_int_value(mrb, SDLK_SPACE));
  mrb_define_const(mrb, cg_mKey, "LEFT_ALT", mrb_int_value(mrb, SDLK_LALT));
  mrb_define_const(mrb, cg_mKey, "RIGHT_ALT", mrb_int_value(mrb, SDLK_RALT));
  mrb_define_const(mrb, cg_mKey, "LEFT_CTRL", mrb_int_value(mrb, SDLK_LCTRL));
  mrb_define_const(mrb, cg_mKey, "RIGHT_CTRL", mrb_int_value(mrb, SDLK_RCTRL));
}
