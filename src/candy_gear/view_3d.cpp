/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "view_3d.hpp"

#include "sprite.hpp"
#include "orientation_3d.hpp"
#include "vector_3d.hpp"
#include "vector_4d.hpp"
#include "../blu_cat/gra/sprite.hpp"
#include "../blu_cat/gra/view_3d.hpp"

void
cg_free_view_3d(mrb_state *mrb, void* obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::View3D>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_view_3d_type = { "CG_View3D", cg_free_view_3d };

static mrb_value
cg_cView3D_initialize(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::vec4> *region;
  mrb_float projection_width, projection_height;
  std::shared_ptr<BluCat::GRA::View3D> *ptr;

  mrb_get_args(mrb, "dff", &region, &cg_vector_4d_type,
               &projection_width, &projection_height);
  ptr = (std::shared_ptr<BluCat::GRA::View3D>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::View3D>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::View3D>));

  new(ptr)std::shared_ptr<BluCat::GRA::View3D>(
    std::make_shared<BluCat::GRA::View3D>(
      *region->get(), projection_width, projection_height));

  mrb_data_init(self, ptr, &cg_view_3d_type);
  return self;
}

static mrb_value
cg_cView3D_set_camera_position(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::vec3> *camera_position;
  auto ptr = (std::shared_ptr<BluCat::GRA::View3D>*)DATA_PTR(self);

  mrb_get_args(mrb, "d", &camera_position, &cg_vector_3d_type);
  (*ptr)->camera_position = (*camera_position);

  return self;
}

static mrb_value
cg_cView3D_set_camera_orientation(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::quat> *camera_orientation;
  auto ptr = (std::shared_ptr<BluCat::GRA::View3D>*)DATA_PTR(self);

  mrb_get_args(mrb, "d", &camera_orientation, &cg_orientation_3d_type);
  (*ptr)->camera_orientation = (*camera_orientation);

  return self;
}

static mrb_value
cg_cView3D_get_field_of_view(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::View3D>*)DATA_PTR(self);

  return mrb_float_value(mrb, (*ptr)->field_of_view);

  return self;
}

static mrb_value
cg_cView3D_set_field_of_view(mrb_state *mrb, mrb_value self)
{
  mrb_float fov;
  auto ptr = (std::shared_ptr<BluCat::GRA::View3D>*)DATA_PTR(self);

  mrb_get_args(mrb, "f", &fov);
  (*ptr)->field_of_view = fov;

  return self;
}

void
cg_view_3d_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cView3D;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cView3D = mrb_define_class_under(mrb, cg_m, "View3D", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cView3D, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cView3D, "initialize", cg_cView3D_initialize, MRB_ARGS_REQ(3));
  mrb_define_method(
    mrb, cg_cView3D, "camera_position=", cg_cView3D_set_camera_position,
    MRB_ARGS_REQ(1));
  mrb_define_method(
    mrb, cg_cView3D, "camera_orientation=", cg_cView3D_set_camera_orientation,
    MRB_ARGS_REQ(1));
  mrb_define_method(
    mrb, cg_cView3D, "field_of_view", cg_cView3D_get_field_of_view,
    MRB_ARGS_NONE());
  mrb_define_method(
    mrb, cg_cView3D, "field_of_view=", cg_cView3D_set_field_of_view,
    MRB_ARGS_REQ(1));
}
