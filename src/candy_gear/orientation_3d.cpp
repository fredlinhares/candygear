/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _USE_MATH_DEFINES

#include "orientation_3d.hpp"

#include "vector_3d.hpp"

void
cg_free_orientation_3d(mrb_state *mrb, void* obj)
{
  auto ptr = static_cast<std::shared_ptr<glm::quat>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_orientation_3d_type = {
  "CG_Orientation3D", cg_free_orientation_3d};

static mrb_value
cg_cOrientation3D_initialize(mrb_state *mrb, mrb_value self)
{
  mrb_float x, y, z;
  std::shared_ptr<glm::quat> *ptr;

  mrb_get_args(mrb, "fff", &x, &y, &z);
  ptr = (std::shared_ptr<glm::quat>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<glm::quat>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<glm::quat>));

	glm::vec3 angles(x, y, z);
  new(ptr)std::shared_ptr<glm::quat>(
    std::make_shared<glm::quat>(angles));
  (**ptr) = glm::normalize(**ptr);

  mrb_data_init(self, ptr, &cg_orientation_3d_type);
  return self;
}

static mrb_value
cg_cOrientation3D_get_w(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::quat> *ptr =
    (std::shared_ptr<glm::quat>*)DATA_PTR(self);

  return mrb_float_value(mrb, (*ptr)->w);
}

static mrb_value
cg_cOrientation3D_get_x(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::quat> *ptr =
    (std::shared_ptr<glm::quat>*)DATA_PTR(self);

  return mrb_float_value(mrb, (*ptr)->x);
}

static mrb_value
cg_cOrientation3D_get_y(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::quat> *ptr =
    (std::shared_ptr<glm::quat>*)DATA_PTR(self);

  return mrb_float_value(mrb, (*ptr)->y);
}

static mrb_value
cg_cOrientation3D_get_z(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::quat> *ptr =
    (std::shared_ptr<glm::quat>*)DATA_PTR(self);

  return mrb_float_value(mrb, (*ptr)->z);
}

static mrb_value
cg_cOrientation3D_rotate(mrb_state *mrb, mrb_value self)
{
  mrb_float x, y, z;
  auto *ptr = (std::shared_ptr<glm::quat>*)DATA_PTR(self);

  mrb_get_args(mrb, "fff", &x, &y, &z);

  glm::vec3 angles(x, y, z);
  glm::quat rot(angles);
  (**ptr) *= rot;

  // TODO: calling normalize for every rotation is expensive.
  (**ptr) = glm::normalize(**ptr);

  return self;
}

void
cg_orientation_3d_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cOrientation3D;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cOrientation3D = mrb_define_class_under(
    mrb, cg_m, "Orientation3D", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cOrientation3D, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cOrientation3D, "initialize", cg_cOrientation3D_initialize,
    MRB_ARGS_REQ(3));

  mrb_define_method(
    mrb, cg_cOrientation3D, "w", cg_cOrientation3D_get_w, MRB_ARGS_NONE());
  mrb_define_method(
    mrb, cg_cOrientation3D, "x", cg_cOrientation3D_get_x, MRB_ARGS_NONE());
  mrb_define_method(
    mrb, cg_cOrientation3D, "y", cg_cOrientation3D_get_y, MRB_ARGS_NONE());
  mrb_define_method(
    mrb, cg_cOrientation3D, "z", cg_cOrientation3D_get_z, MRB_ARGS_NONE());

  mrb_define_method(
    mrb, cg_cOrientation3D, "rotate", cg_cOrientation3D_rotate,
    MRB_ARGS_REQ(3));
}
