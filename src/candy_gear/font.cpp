/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "font.hpp"

#include "../blu_cat/gra/font.hpp"

void
cg_free_font(mrb_state *mrb, void *obj)
{
  auto ptr = static_cast<BluCat::GRA::Font*>(obj);

  ptr->~Font();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type
cg_font_type = {"CG_Font", cg_free_font};

static mrb_value
cg_cFont_initialize(mrb_state *mrb, mrb_value self)
{
  BluCat::GRA::Font *ptr;
  const char *font_path;
  mrb_int font_size;

  mrb_get_args(mrb, "zi", &font_path, &font_size);
  ptr = (BluCat::GRA::Font*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (BluCat::GRA::Font*)mrb_malloc(mrb, sizeof(BluCat::GRA::Font));

  try
  {
    new(ptr)BluCat::GRA::Font(font_path, font_size);
  }
  catch(const std::invalid_argument &e)
  {
    mrb_raise(mrb, E_RUNTIME_ERROR, e.what());
  }

  mrb_data_init(self, ptr, &cg_font_type);
  return self;
}

void
cg_font_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cFont;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cFont = mrb_define_class_under(mrb, cg_m, "Font", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cFont, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cFont, "initialize", cg_cFont_initialize, MRB_ARGS_REQ(2));
}
