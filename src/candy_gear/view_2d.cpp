/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "view_2d.hpp"

#include "sprite.hpp"
#include "vector_4d.hpp"
#include "view_3d.hpp"
#include "../blu_cat/gra/sprite.hpp"
#include "../blu_cat/gra/view_2d.hpp"

void
cg_free_view_2d(mrb_state *mrb, void* obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::View2D>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_view_2d_type = { "CG_View2D", cg_free_view_2d };

static mrb_value
cg_cView2D_initialize(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<glm::vec4> *region;
  mrb_float projection_width, projection_height;
  std::shared_ptr<BluCat::GRA::View2D> *ptr;

  mrb_get_args(mrb, "dff", &region, &cg_vector_4d_type,
               &projection_width, &projection_height);
  ptr = (std::shared_ptr<BluCat::GRA::View2D>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::View2D>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::View2D>));

  new(ptr)std::shared_ptr<BluCat::GRA::View2D>(
    std::make_shared<BluCat::GRA::View2D>(
      *region->get(), projection_width, projection_height));

  mrb_data_init(self, ptr, &cg_view_2d_type);
  return self;
}

BluCat::GRA::View2D*
cg_cView_to_view_2d(mrb_state *mrb, mrb_value view_value)
{
  BluCat::GRA::View2D* view_2d;
  const mrb_data_type *type = DATA_TYPE(view_value);

  if(type == &cg_view_2d_type)
    view_2d = static_cast<std::shared_ptr<BluCat::GRA::View2D>*>(
      DATA_PTR(view_value))->get();
  else if (type == &cg_view_3d_type)
    view_2d = static_cast<BluCat::GRA::View2D*>(
      static_cast<std::shared_ptr<BluCat::GRA::View3D>*>(
        DATA_PTR(view_value))->get());
  else
    mrb_raisef(
      mrb, E_TYPE_ERROR, "wrong argument type %s (expected %s or %s)",
      type->struct_name, cg_view_2d_type.struct_name,
      cg_view_3d_type.struct_name);

  return view_2d;
}

void
cg_view_2d_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cView2D;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cView2D = mrb_define_class_under(mrb, cg_m, "View2D", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cView2D, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cView2D, "initialize", cg_cView2D_initialize, MRB_ARGS_REQ(3));
}
