/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "texture.hpp"

#include "core.hpp"
#include "font.hpp"
#include "../blu_cat/gra/texture.hpp"

void
cg_free_texture(mrb_state *mrb, void* obj)
{
  auto *ptr = static_cast<std::shared_ptr<BluCat::GRA::Texture>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_texture_type = { "CG_Texture", cg_free_texture };

static mrb_value
texture_alloc(mrb_state *mrb, mrb_value klass)
{
  struct RClass *cg_cTexture = mrb_class_ptr(klass);
  enum mrb_vtype ttype = MRB_INSTANCE_TT(cg_cTexture);

  // I do not know if I need this. If things break, try uncomment this line.
  // if (ttype == 0) ttype = MRB_TT_OBJECT;
  return mrb_obj_value(
    (struct RObject*)mrb_obj_alloc(mrb, ttype, cg_cTexture));
}

static mrb_value
cg_cTexture_from_image(mrb_state *mrb, mrb_value self)
{
  struct mrb_value texture = texture_alloc(mrb, self);
  const char *file_path;
  std::shared_ptr<BluCat::GRA::Texture> *ptr;

  mrb_get_args(mrb, "z", &file_path);
  ptr = (std::shared_ptr<BluCat::GRA::Texture>*)DATA_PTR(texture);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::Texture>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::Texture>));
  new(ptr)std::shared_ptr<BluCat::GRA::Texture>(
    std::make_shared<BluCat::GRA::Texture>(file_path));

  mrb_data_init(texture, ptr, &cg_texture_type);
  return texture;
}

mrb_value
cg_cTexture_from_text(mrb_state *mrb, mrb_value self)
{
  const char *text;
  unsigned int width, height;
  struct mrb_value texture = texture_alloc(mrb, self);
  BluCat::GRA::Font *font_ptr;
  std::shared_ptr<BluCat::GRA::Texture> *ptr;

  mrb_get_args(mrb, "dz", &font_ptr, &cg_font_type, &text);

  ptr = (std::shared_ptr<BluCat::GRA::Texture>*)DATA_PTR(texture);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::Texture>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::Texture>));
  new(ptr)std::shared_ptr<BluCat::GRA::Texture>(
    std::make_shared<BluCat::GRA::Texture>(font_ptr, text));

  mrb_data_init(texture, ptr, &cg_texture_type);
  return texture;
}

static mrb_value
cg_cTexture_width(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::Texture>*)DATA_PTR(self);
  return mrb_int_value(mrb, (*ptr)->width);
}

static mrb_value
cg_cTexture_height(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::Texture>*)DATA_PTR(self);
  return mrb_int_value(mrb, (*ptr)->height);
}

void
cg_texture_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cTexture;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cTexture = mrb_define_class_under(
    mrb, cg_m, "Texture", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cTexture, MRB_TT_DATA);

  mrb_undef_class_method(mrb, cg_cTexture, "new");
  mrb_define_class_method(
    mrb, cg_cTexture, "from_image", cg_cTexture_from_image,
    MRB_ARGS_REQ(1) | MRB_ARGS_OPT(1));
  mrb_define_class_method(
    mrb, cg_cTexture, "from_text", cg_cTexture_from_text, MRB_ARGS_REQ(2));
  mrb_define_method(
    mrb, cg_cTexture, "width", cg_cTexture_width, MRB_ARGS_NONE());
  mrb_define_method(
    mrb, cg_cTexture, "height", cg_cTexture_height, MRB_ARGS_NONE());
}
