/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sprite_3d.hpp"

#include "sprite.hpp"
#include "vector_3d.hpp"

void
cg_free_sprite_3d(mrb_state *mrb, void *obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::Sprite3D>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_sprite_3d_type = {
  "CG_Sprite3D", cg_free_sprite_3d };

static mrb_value
cg_cSprite3D_initialize(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<BluCat::GRA::Sprite> *sprite;
  std::shared_ptr<glm::vec3> *position;
  mrb_float w, h;
  std::shared_ptr<BluCat::GRA::Sprite3D> *ptr;

  mrb_get_args(mrb, "ddff", &sprite, &cg_sprite_type,
	       &position, &cg_vector_3d_type, &w, &h);
  ptr = (std::shared_ptr<BluCat::GRA::Sprite3D>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::Sprite3D>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::Sprite3D>));

  glm::vec3 size{w, h, 0.0};
  new(ptr)std::shared_ptr<BluCat::GRA::Sprite3D>(
    std::make_shared<BluCat::GRA::Sprite3D>(*sprite, *position, size));

  mrb_data_init(self, ptr, &cg_sprite_3d_type);
  return self;
}

static mrb_value
cg_cSprite3D_draw(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::Sprite3D>*)DATA_PTR(self);

  auto &sprites_3d_to_draw = BluCat::INT::core.vk_renderer->sprites_3d_to_draw[
    BluCat::INT::core.vk_swapchain->current_frame];
  sprites_3d_to_draw.emplace_back(*ptr);

  return self;
}

void
cg_sprite_3d_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cSprite3D;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cSprite3D = mrb_define_class_under(
    mrb, cg_m, "Sprite3D", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cSprite3D, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cSprite3D, "initialize", cg_cSprite3D_initialize, MRB_ARGS_REQ(3));
  mrb_define_method(
    mrb, cg_cSprite3D, "draw", cg_cSprite3D_draw, MRB_ARGS_NONE());
}
