/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CANDY_GEAR_VIEW_2D_H
#define CANDY_GEAR_VIEW_2D_H 1

#include "core.hpp"

extern const struct mrb_data_type cg_view_2d_type;

// Receives a mrb_value that points to a View2D or a View3D and returns a
// pointer to View2D. If the mrb_value points to a View3D, the view will be
// cast to a BluCat::GRA::View2D.
BluCat::GRA::View2D*
cg_cView_to_view_2d(mrb_state *mrb, mrb_value view_value);

void
cg_view_2d_init(mrb_state *mrb);

#endif /* CANDY_GEAR_VIEW_2D_H */
