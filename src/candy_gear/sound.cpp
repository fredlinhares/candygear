/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sound.hpp"

void
cg_free_sound(mrb_state *mrb, void* obj)
{
  struct cg_sound *ptr = static_cast<cg_sound*>(obj);

  Mix_FreeChunk(ptr->chunk);
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_sound_type = {
  "CG_Sound", cg_free_sound };

static mrb_value
cg_cSound_initialize(mrb_state *mrb, mrb_value self)
{
  const char *file_path;

  struct cg_sound *ptr;

  mrb_get_args(mrb, "z", &file_path);
  ptr = (struct cg_sound *)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (struct cg_sound *)mrb_malloc(mrb, sizeof(struct cg_sound));

  ptr->chunk = Mix_LoadWAV(file_path);
  if(ptr->chunk == NULL) mrb_raise(mrb, E_RUNTIME_ERROR, Mix_GetError());

  mrb_data_init(self, ptr, &cg_sound_type);
  return self;
}

static mrb_value
cg_cSound_play(mrb_state *mrb, mrb_value self)
{
  struct cg_sound *ptr;

  ptr = (struct cg_sound *)DATA_PTR(self);

  Mix_PlayChannel(-1, ptr->chunk, 0);

  return self;
}

void
cg_sound_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cSound;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cSound = mrb_define_class_under(mrb, cg_m, "Sound", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cSound, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cSound, "initialize", cg_cSound_initialize, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, cg_cSound, "play", cg_cSound_play, MRB_ARGS_NONE());
}
