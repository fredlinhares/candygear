/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <chrono>
#include <thread>

#include "core.hpp"

cg_sCore cg_core;

int main(int argc, char *argv[])
{
  using namespace std::chrono;
  using namespace std::this_thread;

  SDL_Event event;

  // Random numbers
  BluCat::INT::random_number_generator.seed(BluCat::INT::random_seed());

  cg_core.game_file = argv[1];
  cg_core.mrb = mrb_open();
  if (!cg_core.mrb) throw CommandError{"Failed to initialize mruby."};
  try{ cg_sCore::loader.execute(nullptr); }
  catch(const CommandError &error)
  {
    BluCat::INT::core.log.message(Log::Level::Fatal, error.what());
    mrb_close(cg_core.mrb);
    return 1;
  }

  mrb_value main_obj{mrb_top_self(cg_core.mrb)};

  mrb_funcall_id(cg_core.mrb, main_obj, cg_core.sym_init, 0);
  if (cg_core.mrb->exc)
  {
    mrb_print_error(cg_core.mrb);
		mrb_close(cg_core.mrb);
    cg_sCore::loader.revert(nullptr);
    return 1;
  }

  auto frame_start = steady_clock::now();

  // Game main loop.
  while(!cg_core.quit_game)
  {
    // Get input.
    while(SDL_PollEvent(&event) != 0)
    {
      switch(event.type)
      {
      case SDL_KEYDOWN:
        mrb_funcall_id(
          cg_core.mrb, main_obj, cg_core.sym_key_down, 1,
          mrb_int_value(cg_core.mrb, event.key.keysym.sym));
        break;
      case SDL_KEYUP:
        mrb_funcall_id(
          cg_core.mrb, main_obj, cg_core.sym_key_up, 1,
          mrb_int_value(cg_core.mrb, event.key.keysym.sym));
        break;
      case SDL_MOUSEMOTION:
        break;
      case SDL_MOUSEBUTTONDOWN:
        break;
      case SDL_MOUSEBUTTONUP:
        break;
      case SDL_QUIT:
        mrb_funcall_id(cg_core.mrb, main_obj, cg_core.sym_quit, 0);
        break;
      }
    }

    mrb_funcall_id(cg_core.mrb, main_obj, cg_core.sym_tick, 0);
    if (cg_core.mrb->exc)
    {
      mrb_print_error(cg_core.mrb);
      cg_core.quit_game = true;
    }
    else
    {
      BluCat::INT::core.vk_renderer->draw();

      { // Timer
        auto frame_stop = steady_clock::now();
        auto frame_duration = frame_stop - frame_start;

        // If frame take less time than maximum allowed.
        if(BluCat::INT::core.max_frame_duration > frame_duration)
          sleep_for(BluCat::INT::core.max_frame_duration - frame_duration);

        frame_start = frame_stop;
      }
    }
  }

  mrb_close(cg_core.mrb);
  cg_sCore::loader.revert(nullptr);

  return 0;
}
