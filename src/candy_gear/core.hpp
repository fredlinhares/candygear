/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CANDY_GEAR_CORE_H
#define CANDY_GEAR_CORE_H 1

#include <mruby.h>
#include <mruby/class.h>
#include <mruby/compile.h>
#include <mruby/data.h>
#include <mruby/dump.h>
#include <mruby/variable.h>

#define SDL_MAIN_HANDLED

#ifdef _WIN64
#include <Windows.h>
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <SDL2/SDL_mixer.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "../blu_cat/int/core.hpp"

/**
 * The Core class stores all global states that the engine needs to work.
 * Global variables are not evil if you use them carefully.
 */
struct cg_sCore
{
  static const CommandChain loader;

  mrb_state *mrb;

  std::string game_file;

  /// mruby symbols
  mrb_sym sym_config, sym_debug, sym_error, sym_fatal, sym_information,
    sym_init, sym_key_down, sym_key_up, sym_quit, sym_tick, sym_trace,
    sym_warning;

  bool quit_game;

  SDL_Window *window;
};

extern cg_sCore cg_core;

#endif /* CANDY_GEAR_CORE_H */
