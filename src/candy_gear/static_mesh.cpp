/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "static_mesh.hpp"

#include "orientation_3d.hpp"
#include "vector_3d.hpp"
#include "../blu_cat/gra/static_mesh.hpp"

void
cg_free_static_mesh(mrb_state *mrb, void* obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::StaticMesh>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_static_mesh_type = {
  "CG_StaticMesh", cg_free_static_mesh };

static mrb_value
cg_cStaticMesh_initialize(mrb_state *mrb, mrb_value self)
{
  const char *file_path;

  std::shared_ptr<BluCat::GRA::StaticMesh> *ptr;

  mrb_get_args(mrb, "z", &file_path);
  ptr = (std::shared_ptr<BluCat::GRA::StaticMesh>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::StaticMesh>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::StaticMesh>));

  new(ptr)std::shared_ptr<BluCat::GRA::StaticMesh>(
    std::make_shared<BluCat::GRA::StaticMesh>(file_path));

  mrb_data_init(self, ptr, &cg_static_mesh_type);
  return self;
}

void
cg_static_mesh_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cStaticMesh;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cStaticMesh = mrb_define_class_under(
    mrb, cg_m, "StaticMesh", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cStaticMesh, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cStaticMesh, "initialize", cg_cStaticMesh_initialize,
    MRB_ARGS_REQ(1));
}
