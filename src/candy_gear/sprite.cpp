/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sprite.hpp"

#include "texture.hpp"
#include "vector_4d.hpp"
#include "view_2d.hpp"
#include "../blu_cat/gra/sprite.hpp"

void
cg_free_sprite(mrb_state *mrb, void* obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::Sprite>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_sprite_type = { "CG_Sprite", cg_free_sprite };

static mrb_value
cg_cSprite_initialize(mrb_state *mrb, mrb_value self)
{
  mrb_float x, y, w, h;
  glm::vec4 vector_4d;
  std::shared_ptr<BluCat::GRA::Texture> *texture;
  std::shared_ptr<BluCat::GRA::Sprite> *ptr;

  mrb_get_args(
    mrb, "dffff", &texture, &cg_texture_type, &x, &y, &w, &h);
  ptr = (std::shared_ptr<BluCat::GRA::Sprite>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::Sprite>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::Sprite>));

  vector_4d.x = x;
  vector_4d.y = y;
  vector_4d.z = w;
  vector_4d.w = h;
  new(ptr)std::shared_ptr<BluCat::GRA::Sprite>(
    std::make_shared<BluCat::GRA::Sprite>(*texture, vector_4d));

  mrb_data_init(self, ptr, &cg_sprite_type);
  return self;
}

static mrb_value
cg_cSprite_draw(mrb_state *mrb, mrb_value self)
{
  mrb_value view_value;
  BluCat::GRA::View2D *view_2d;
  mrb_float x, y, w, h, z_index{0.0};
  auto ptr = (std::shared_ptr<BluCat::GRA::Sprite>*)DATA_PTR(self);

  mrb_get_args(mrb, "offff|f", &view_value, &x, &y, &w, &h, &z_index);

  view_2d = cg_cView_to_view_2d(mrb, view_value);

  glm::vec4 rect(x, y, x + w, y + h);
  auto &sprites_to_draw = view_2d->sprites_to_draw[
    BluCat::INT::core.vk_swapchain->current_frame];
  sprites_to_draw.emplace_back(*ptr, rect, z_index);

  return self;
}

void
cg_sprite_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cSprite;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cSprite = mrb_define_class_under(mrb, cg_m, "Sprite", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cSprite, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cSprite, "initialize", cg_cSprite_initialize, MRB_ARGS_REQ(5));
  mrb_define_method(mrb, cg_cSprite, "draw", cg_cSprite_draw, MRB_ARGS_REQ(5)|
    MRB_ARGS_OPT(1));
}
