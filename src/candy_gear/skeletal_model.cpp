/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "skeletal_model.hpp"

#include "orientation_3d.hpp"
#include "vector_3d.hpp"
#include "skeletal_mesh.hpp"
#include "texture.hpp"
#include "../blu_cat/gra/skeletal_model.hpp"

void
cg_free_skeletal_model(mrb_state *mrb, void *obj)
{
  auto ptr = static_cast<std::shared_ptr<BluCat::GRA::SkeletalModel>*>(obj);

  ptr->~shared_ptr();
  mrb_free(mrb, ptr);
}

const struct mrb_data_type cg_skeletal_model_type = {
  "CG_SkeletalModel", cg_free_skeletal_model };

static mrb_value
cg_cSkeletalModel_initialize(mrb_state *mrb, mrb_value self)
{
  std::shared_ptr<BluCat::GRA::SkeletalMesh> *skeletal_mesh;
  std::shared_ptr<BluCat::GRA::Texture> *texture;
  std::shared_ptr<glm::vec3> *position;
  std::shared_ptr<glm::quat> *orientation;
  std::shared_ptr<BluCat::GRA::SkeletalModel> *ptr;

  mrb_get_args(
    mrb, "dddd", &skeletal_mesh, &cg_skeletal_mesh_type, &texture,
    &cg_texture_type, &position, &cg_vector_3d_type, &orientation,
    &cg_orientation_3d_type);
  ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)DATA_PTR(self);
  if(ptr) mrb_free(mrb, ptr);
  ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)mrb_malloc(
    mrb, sizeof(std::shared_ptr<BluCat::GRA::SkeletalModel>));

  new(ptr)std::shared_ptr<BluCat::GRA::SkeletalModel>(
    std::make_shared<BluCat::GRA::SkeletalModel>(
      *skeletal_mesh, *texture, *position, *orientation));

  mrb_data_init(self, ptr, &cg_skeletal_model_type);
  return self;
}

static mrb_value
cg_cSkeletalModel_set_orientation(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)DATA_PTR(self);
  std::shared_ptr<glm::quat> *orientation;

  mrb_get_args(mrb, "d", &orientation, &cg_orientation_3d_type);
  (*ptr)->orientation = *orientation;

  return self;
}

static mrb_value
cg_cSkeletalModel_set_position(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)DATA_PTR(self);
  std::shared_ptr<glm::vec3> *position;

  mrb_get_args(mrb, "d", &position, &cg_vector_3d_type);
  (*ptr)->position = *position;

  return self;
}

static mrb_value
cg_cSkeletalModel_set_animation(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)DATA_PTR(self);
  mrb_int animation_index;

  mrb_get_args(mrb, "i", &animation_index);
  (*ptr)->animation_index = animation_index;

  return self;
}

static mrb_value
cg_cSkeletalModel_draw(mrb_state *mrb, mrb_value self)
{
  auto ptr = (std::shared_ptr<BluCat::GRA::SkeletalModel>*)DATA_PTR(self);

  auto &instances = BluCat::INT::core.vk_renderer->skeletal_models_to_draw[
    BluCat::INT::core.vk_swapchain->current_frame][(*ptr)->skeletal_mesh];
  instances.push_back(*ptr);

  return self;
}

void
cg_skeletal_model_init(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_cSkeletalModel;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_cSkeletalModel = mrb_define_class_under(
    mrb, cg_m, "SkeletalModel", mrb->object_class);
  MRB_SET_INSTANCE_TT(cg_cSkeletalModel, MRB_TT_DATA);
  mrb_define_method(
    mrb, cg_cSkeletalModel, "initialize", cg_cSkeletalModel_initialize,
    MRB_ARGS_REQ(4));
  mrb_define_method(
    mrb, cg_cSkeletalModel, "position=", cg_cSkeletalModel_set_position,
    MRB_ARGS_REQ(1));
  mrb_define_method(
    mrb, cg_cSkeletalModel, "orientation=", cg_cSkeletalModel_set_orientation,
    MRB_ARGS_REQ(1));
  mrb_define_method(
    mrb, cg_cSkeletalModel, "animation=", cg_cSkeletalModel_set_animation,
    MRB_ARGS_REQ(1));
  mrb_define_method(
    mrb, cg_cSkeletalModel, "draw", cg_cSkeletalModel_draw, MRB_ARGS_NONE());
}
