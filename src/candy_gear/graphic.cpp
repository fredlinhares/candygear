/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graphic.hpp"

#include "core.hpp"

static mrb_value
cg_mCandyGear_set_display_width(mrb_state *mrb, mrb_value self)
{
  mrb_int width;

  mrb_get_args(mrb, "i", &width);
  BluCat::INT::core.display_width = width;

  return self;
}

static mrb_value
cg_mCandyGear_set_display_height(mrb_state *mrb, mrb_value self)
{
  mrb_int height;

  mrb_get_args(mrb, "i", &height);
  BluCat::INT::core.display_height = height;

  return self;
}

static mrb_value
cg_mCandyGear_set_fps(mrb_state *mrb, mrb_value self)
{
  mrb_int fps;

  mrb_get_args(mrb, "i", &fps);
  BluCat::INT::core.fps = fps;

  return self;
}

void
cg_graphic_init_config(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_mGraphic;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_mGraphic = mrb_define_module_under(mrb, cg_m, "Graphic");

  mrb_define_class_method(
    mrb, cg_mGraphic, "display_width=", cg_mCandyGear_set_display_width,
    MRB_ARGS_REQ(1));
  mrb_define_class_method(
    mrb, cg_mGraphic, "display_height=", cg_mCandyGear_set_display_height,
    MRB_ARGS_REQ(1));
  mrb_define_class_method(
    mrb, cg_mGraphic, "fps=", cg_mCandyGear_set_fps, MRB_ARGS_REQ(1));
}

void
cg_graphic_finish_config(mrb_state *mrb)
{
  struct RClass *cg_m, *cg_mGraphic;

  cg_m = mrb_module_get(mrb, "CandyGear");
  cg_mGraphic = mrb_module_get_under(mrb, cg_m, "Graphic");

  mrb_undef_class_method(mrb, cg_mGraphic, "display_width=");
  mrb_undef_class_method(mrb, cg_mGraphic, "display_height=");
  mrb_undef_class_method(mrb, cg_mGraphic, "fps=");
}
