/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SKELETAL_MODEL_H
#define BLU_CAT_GRA_SKELETAL_MODEL_H 1

#include <memory>
#include <vector>

#include "vulkan.hpp"
#include "skeletal_mesh.hpp"

namespace BluCat::GRA
{

struct SkeletalModel
{
  std::shared_ptr<SkeletalMesh> skeletal_mesh;
  std::shared_ptr<Texture> texture;
  std::vector<UniformBuffer> uniform_buffers;
  std::shared_ptr<glm::vec3> position;
  std::shared_ptr<glm::quat> orientation;
  int animation_index;
  float animation_time;
  std::vector<glm::mat4> bone_transforms;

  VkDescriptorPool descriptor_pool;
  std::vector<VkDescriptorSet> descriptor_sets;

  SkeletalModel(
    std::shared_ptr<SkeletalMesh> skeletal_mesh,
    std::shared_ptr<Texture> texture, std::shared_ptr<glm::vec3> position,
    std::shared_ptr<glm::quat> orientation);
  ~SkeletalModel();

  void
  tick(float delta);
};

}

#endif /* BLU_CAT_GRA_SKELETAL_MODEL_H */
