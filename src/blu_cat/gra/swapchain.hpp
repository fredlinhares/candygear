/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SWAPCHAIN_H
#define BLU_CAT_GRA_SWAPCHAIN_H 1

#include "../com/command.hpp"
#include "vulkan.hpp"

namespace BluCat::GRA
{

struct Swapchain
{
  VkSwapchainKHR swapchain;
  VkFormat image_format;

  uint32_t images_count;
  VkImage *images;
  VkImageView *image_views;

  static const int max_frames_in_flight{2};
  size_t current_frame;
  std::vector<VkSemaphore> image_available_semaphores;
  std::vector<VkSemaphore> render_finished_semaphores;
  std::vector<VkFence> in_flight_fences;

  Swapchain();
  ~Swapchain();
};

}

#endif /* BLU_CAT_GRA_SWAPCHAIN_H */
