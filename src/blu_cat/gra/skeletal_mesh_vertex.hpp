/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SKELETAL_MESH_VERTEX_H
#define BLU_CAT_GRA_SKELETAL_MESH_VERTEX_H 1

#include "vulkan.hpp"

namespace BluCat::GRA
{

// This variable define the maximum ammount of bones that can influence a
// vertex.
const int SKELETAL_MESH_MAX_NUM_OF_INFLUENCING_BONES{4};
const int SKELETAL_MESH_MAX_NUM_OF_BONES{50};

struct SkeletalMeshVertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 texture_coord;

  uint32_t bone_ids[SKELETAL_MESH_MAX_NUM_OF_INFLUENCING_BONES];
  float bone_weights[SKELETAL_MESH_MAX_NUM_OF_INFLUENCING_BONES];
};

}

#endif /* BLU_CAT_GRA_SKELETAL_MESH_VERTEX_H */
