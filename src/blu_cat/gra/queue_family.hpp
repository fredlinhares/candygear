/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_QUEUE_FAMILY_H
#define BLU_CAT_GRA_QUEUE_FAMILY_H 1

#include <mutex>
#include <vector>

#include "vulkan.hpp"
#include "queue.hpp"

namespace BluCat::GRA
{
class Device;

struct QueueState
{
  VkQueue queue;
  bool busy;
};

class QueueFamily
{
  friend class Queue;

  std::mutex queue_mutex;
  std::vector<QueueState> queue_states;

public:
  BluCat::GRA::Device *device;

  uint32_t family_index;
  VkQueueFamilyProperties family_properties;

  QueueFamily(BluCat::GRA::Device *device, uint32_t family_index,
	      const VkQueueFamilyProperties &queue_family_properties);

  Queue
  get_queue();
};

}

#endif /* BLU_CAT_GRA_QUEUE_FAMILY_H */
