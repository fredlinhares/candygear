/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sprite_to_draw.hpp"

namespace BluCat::GRA
{
  SpriteToDraw::SpriteToDraw(
    std::shared_ptr<Sprite> sprite, const glm::vec4 &position, float z_index):
    sprite{sprite},
    position{position},
    z_index{z_index}
  {
  }

  bool
  operator<(const SpriteToDraw& a, const SpriteToDraw& b)
  {
    return a.z_index < b.z_index;
  }

  bool
  operator>(const SpriteToDraw& a, const SpriteToDraw& b)
  {
    return a.z_index > b.z_index;
  }
}
