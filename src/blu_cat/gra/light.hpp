/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_LIGHT_H
#define BLU_CAT_GRA_LIGHT_H 1

#include "vulkan.hpp"
#include "uniform_buffer.hpp"

namespace BluCat::GRA
{

struct Light
{
  // FIXME: if this vector get resized, it will cause a segmentation fault!
  std::vector<UniformBuffer> ub_world_vert;
  std::vector<UniformBuffer> ub_world_frag;

  VkDescriptorPool descriptor_pool;
  std::vector<VkDescriptorSet> descriptor_sets_world;

  Light();
  ~Light();
};

}

#endif /* BLU_CAT_GRA_LIGHT_H */
