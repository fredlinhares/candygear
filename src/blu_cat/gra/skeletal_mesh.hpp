/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SKELETAL_MESH_H
#define BLU_CAT_GRA_SKELETAL_MESH_H 1

#include <string>
#include <vector>

#include "animation.hpp"
#include "vulkan.hpp"
#include "destination_buffer.hpp"
#include "queue_family.hpp"
#include "uniform_buffer.hpp"
#include "texture.hpp"

namespace BluCat::GRA
{

struct SkeletalMesh
{
  QueueFamily *queue_family;

  uint32_t index_count;
  SourceBuffer *source_vertex_buffer;
  DestinationBuffer *index_buffer;
  DestinationBuffer *vertex_buffer;

  std::vector<Bone> bones;
  std::vector<Animation> animations;

  SkeletalMesh(std::string mesh_path);
  SkeletalMesh(const char* mesh_path);
  ~SkeletalMesh();
};

}

#endif /* BLU_CAT_GRA_SKELETAL_MESH_H */
