/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_GRAPHICS_PIPELINE_2D_WIRED_H
#define BLU_CAT_GRA_GRAPHICS_PIPELINE_2D_WIRED_H 1

#include <memory>

#include "vulkan.hpp"
#include "view_2d.hpp"

namespace BluCat::GRA
{

struct GraphicsPipeline2DWired
{
  QueueFamily *queue_family;

  VkPipeline graphic_pipeline;

  DestinationBuffer *index_buffer;

  GraphicsPipeline2DWired();
  ~GraphicsPipeline2DWired();

  void
  draw(std::shared_ptr<View2D> view, const VkCommandBuffer draw_command_buffer,
       const size_t current_frame, const size_t next_frame,
       const uint32_t image_index);
};

}

#endif /* BLU_CAT_GRA_GRAPHICS_PIPELINE_2D_WIRED_H */
