/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_IMAGE_H
#define BLU_CAT_GRA_IMAGE_H 1

#include <memory>
#include <string>

#include "vulkan.hpp"
#include "device.hpp"

namespace BluCat::GRA::Image
{

struct Error: public std::exception
{
  Error(const std::string &m);
  Error(const char &m);

  const char*
  what() const noexcept;

private:
  std::string error;
};

void
create(
  BluCat::GRA::Device *device,
  VkImage *image,
  VkDeviceMemory *image_memory,
  VkFormat format,
  const VkExtent3D &extent3d,
  uint32_t mip_levels,
  VkImageTiling image_tiling,
  VkImageUsageFlags usage);

void
move_image_state(
  VkCommandBuffer vk_command_buffer,
  VkImage vk_image,
  VkFormat format,
  VkAccessFlags src_access_mask,
  VkAccessFlags dst_access_mask,
  VkImageLayout old_layout,
  VkImageLayout new_layout,
  VkPipelineStageFlags source_stage,
  VkPipelineStageFlags destination_stage);

void
create_view(
  BluCat::GRA::Device *device,
  VkImageView *image_view,
  const VkImage &image,
  VkFormat format,
  VkImageAspectFlags image_aspect_flags);
}

#endif /* BLU_CAT_GRA_IMAGE_H */
