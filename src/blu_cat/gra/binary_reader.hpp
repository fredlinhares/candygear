/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include <string>

#include "vulkan.hpp"

class BinaryReader
{
  int _pointer;
  int _size;
  uint8_t *data;

public:

  BinaryReader(const std::string file_path);
  BinaryReader(const char *file_path);
  ~BinaryReader();

  inline int
  pointer(){return this->_pointer;};

  inline int
  size(){return this->_size;};

  uint8_t
  read_ui8();

  uint32_t
  read_ui32();

  uint64_t
  read_ui64();

  float
  read_float();

  double
  read_double();

  glm::vec2
  read_vec2();

  glm::vec3
  read_vec3();

  glm::quat
  read_quat();

  glm::mat4
  read_mat4();

  void
  read_chars(char *str, int size);
};
