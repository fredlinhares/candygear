/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "source_buffer.hpp"

#include <cstring>

namespace BluCat::GRA
{

SourceBuffer::SourceBuffer(Device *device, void *data, size_t data_size)
{
  this->device = device;
  this->device_size = data_size;
  this->buffer_usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
  this->memory_properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
    VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  try
  {
    BluCat::GRA::BaseBuffer::loader.execute(static_cast<BluCat::GRA::BaseBuffer*>(this));
  }
  catch(const CommandError &command_error)
  {
    std::string error{"Could not initialize Vulkan source buffer → "};
    error += command_error.what();
    throw std::runtime_error{error};
  }
  this->copy_data(data);
}

SourceBuffer::SourceBuffer(SourceBuffer &&that)
{
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;
}

SourceBuffer&
SourceBuffer::operator=(SourceBuffer &&that)
{
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;

  return *this;
}

SourceBuffer::~SourceBuffer()
{
  BluCat::GRA::BaseBuffer::loader.revert(static_cast<BluCat::GRA::BaseBuffer*>(this));
}

void
SourceBuffer::copy_data(void *src_data)
{
  void *dst_data;
  vkMapMemory(this->device->device, this->device_memory, 0, this->device_size,
	      0, &dst_data);
  memcpy(dst_data, src_data, static_cast<size_t>(this->device_size));
  vkUnmapMemory(this->device->device, this->device_memory);
}

}
