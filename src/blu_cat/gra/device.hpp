/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_DEVICE_H
#define BLU_CAT_GRA_DEVICE_H 1

#include <cstdlib>
#include <vector>

#include "vulkan.hpp"
#include "queue_family.hpp"

namespace BluCat::GRA
{

struct Device
{
  uint32_t queue_families_count;
  QueueFamily *queue_families;
  std::vector<QueueFamily*> queue_families_with_graphics;
  std::vector<QueueFamily*> queue_families_with_presentation;

public:
  VkDevice device;
  VkPhysicalDevice physical_device;

  VkShaderModule vert_sprite_3d_shader_module;
  VkShaderModule frag_sprite_3d_shader_module;
  VkShaderModule vert3d_shader_module;
  VkShaderModule vert3d_skeletal_shader_module;
  VkShaderModule frag3d_shader_module;
  VkShaderModule vert2d_solid_shader_module;
  VkShaderModule frag2d_solid_shader_module;
  VkShaderModule vert2d_wired_shader_module;
  VkShaderModule frag2d_wired_shader_module;

  bool with_swapchain;

  Device(VkPhysicalDevice vk_physical_device, bool with_swapchain);
  ~Device();

  bool
  select_memory_type(
    uint32_t *memoryTypeIndex, VkMemoryRequirements *vk_memory_requirements,
    VkMemoryPropertyFlags vk_property_flags);

  QueueFamily*
  get_queue_family_with_graphics() const;

  QueueFamily*
  get_queue_family_with_presentation() const;
};

}

#endif /* BLU_CAT_GRA_DEVICE_H */
