/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_DESTINATION_BUFFER_H
#define BLU_CAT_GRA_DESTINATION_BUFFER_H 1

#include "base_buffer.hpp"
#include "vulkan.hpp"
#include "source_buffer.hpp"
#include "queue_family.hpp"

namespace BluCat::GRA
{

struct DestinationBuffer: public BaseBuffer
{
  DestinationBuffer(const DestinationBuffer &t) = delete;
  DestinationBuffer&
  operator=(const DestinationBuffer &t) = delete;

  QueueFamily *queue_family;
  SourceBuffer *source_buffer;
  VkCommandBuffer vk_command_buffer;

  DestinationBuffer(
    QueueFamily *queue_family, SourceBuffer *source_buffer,
    VkBufferUsageFlags buffer_usage);

  DestinationBuffer(DestinationBuffer &&that);
  DestinationBuffer&
  operator=(DestinationBuffer &&that);

  ~DestinationBuffer();

  void
  copy_data();
};

}

#endif /* BLU_CAT_GRA_DESTINATION_BUFFER_H */
