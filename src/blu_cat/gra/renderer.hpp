/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_RENDERER_H
#define BLU_CAT_GRA_RENDERER_H 1

#include <initializer_list>
#include <memory>
#include <vector>

#include "vulkan.hpp"
#include "skeletal_mesh.hpp"
#include "skeletal_model.hpp"
#include "sprite_3d.hpp"
#include "static_mesh.hpp"
#include "static_model.hpp"
#include "queue_family.hpp"
#include "view_2d.hpp"
#include "view_3d.hpp"

namespace BluCat::GRA
{

struct Renderer
{
  std::vector<
    std::unordered_map<
      std::shared_ptr<SkeletalMesh>,
      std::vector<std::shared_ptr<SkeletalModel>>>>
  skeletal_models_to_draw;

  std::vector<
    std::unordered_map<
      std::shared_ptr<StaticMesh>,
      std::vector<std::shared_ptr<StaticModel>>>>
  static_models_to_draw;

  std::vector<std::vector<std::shared_ptr<Sprite3D>>> sprites_3d_to_draw;

  VkDescriptorPool descriptor_pool;
  std::vector<std::shared_ptr<View2D>> views_2d;
  std::vector<std::shared_ptr<View3D>> views_3d;
  QueueFamily *queue_family;
  VkCommandPool command_pool;
  std::vector<VkCommandBuffer> draw_command_buffers;

  Renderer(std::vector<std::shared_ptr<View2D>> views_2d,
           std::vector<std::shared_ptr<View3D>> views_3d);
  Renderer(std::initializer_list<std::shared_ptr<View2D>> views_2d,
           std::initializer_list<std::shared_ptr<View3D>> views_3d);
  ~Renderer();

	void
	wait_frame();

  void
  draw();
};

}

#endif /* BLU_CAT_GRA_RENDERER_H */
