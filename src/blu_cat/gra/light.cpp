/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "light.hpp"

#include <array>

#include "../int/core.hpp"
#include "uniform_data_object.hpp"

namespace
{

void
load_world_vert_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  try
  {
    self->ub_world_vert.reserve(BluCat::INT::core.vk_swapchain->images_count);
    for(auto i{0}; i < BluCat::INT::core.vk_swapchain->images_count; i++)
      self->ub_world_vert.emplace_back(
	BluCat::INT::core.vk_device_with_swapchain, sizeof(BluCat::GRA::UDOWorld3D_Vert));
  }
  catch(const std::exception& e)
  {
    throw CommandError{e.what()};
  }
}

void
unload_world_vert_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  self->ub_world_vert.clear();
}

void
load_world_frag_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  try
  {
    self->ub_world_frag.reserve(BluCat::INT::core.vk_swapchain->images_count);
    for(auto i{0}; i < BluCat::INT::core.vk_swapchain->images_count; i++)
      self->ub_world_frag.emplace_back(
	BluCat::INT::core.vk_device_with_swapchain, sizeof(BluCat::GRA::UDOWorld3D_Frag));
  }
  catch(const std::exception& e)
  {
    throw CommandError{e.what()};
  }
}

void
unload_world_frag_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  self->ub_world_frag.clear();
}

void
load_descriptor_pool(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  uint32_t uniform_buffers_count =
    self->ub_world_vert.size() + self->ub_world_vert.size();

  VkDescriptorPoolSize descriptor_pool_size{};
  descriptor_pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  descriptor_pool_size.descriptorCount = uniform_buffers_count;

  VkDescriptorPoolCreateInfo pool_info{};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.pNext = nullptr;
  pool_info.flags = 0;
  pool_info.maxSets = uniform_buffers_count;
  pool_info.poolSizeCount = 1;
  pool_info.pPoolSizes = &descriptor_pool_size;

  if(vkCreateDescriptorPool(
       BluCat::INT::core.vk_device_with_swapchain->device, &pool_info, nullptr,
       &self->descriptor_pool) != VK_SUCCESS)
    throw CommandError{"Failed to create a Vulkan descriptor pool."};
}

void
unload_descriptor_pool(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  vkDestroyDescriptorPool(
    BluCat::INT::core.vk_device_with_swapchain->device, self->descriptor_pool,
    nullptr);
}

void
load_descriptor_sets_world(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  std::vector<VkDescriptorSetLayout> layouts(
    BluCat::INT::core.vk_swapchain->images_count,
    BluCat::INT::core.vk_descriptor_set_layout->world);

  VkDescriptorSetAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = self->descriptor_pool;
  alloc_info.descriptorSetCount = layouts.size();
  alloc_info.pSetLayouts = layouts.data();

  self->descriptor_sets_world.resize(layouts.size());
  if(vkAllocateDescriptorSets(
       BluCat::INT::core.vk_device_with_swapchain->device, &alloc_info,
       self->descriptor_sets_world.data()) != VK_SUCCESS)
    throw CommandError{"Failed to create Vulkan world descriptor set."};
}

void
load_resources_to_descriptor_sets(void *obj)
{
  auto self = static_cast<BluCat::GRA::Light*>(obj);

  for(auto i{0}; i < BluCat::INT::core.vk_swapchain->images_count; i++)
  {
    VkDescriptorBufferInfo world_vert_info{};
    world_vert_info.buffer = self->ub_world_vert[i].buffer;
    world_vert_info.offset = 0;
    world_vert_info.range = sizeof(BluCat::GRA::UDOWorld3D_Vert);

    VkDescriptorBufferInfo world_frag_info{};
    world_frag_info.buffer = self->ub_world_frag[i].buffer;
    world_frag_info.offset = 0;
    world_frag_info.range = sizeof(BluCat::GRA::UDOWorld3D_Frag);

    std::array<VkWriteDescriptorSet, 2> write_descriptors{};
    write_descriptors[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptors[0].dstSet = self->descriptor_sets_world[i];
    write_descriptors[0].dstBinding = 0;
    write_descriptors[0].dstArrayElement = 0;
    write_descriptors[0].descriptorCount = 1;
    write_descriptors[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptors[0].pBufferInfo = &world_vert_info;
    write_descriptors[0].pImageInfo = nullptr;
    write_descriptors[0].pTexelBufferView = nullptr;

    write_descriptors[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptors[1].dstSet = self->descriptor_sets_world[i];
    write_descriptors[1].dstBinding = 1;
    write_descriptors[1].dstArrayElement = 0;
    write_descriptors[1].descriptorCount = 1;
    write_descriptors[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptors[1].pBufferInfo = &world_frag_info;
    write_descriptors[1].pImageInfo = nullptr;
    write_descriptors[1].pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(
      BluCat::INT::core.vk_device_with_swapchain->device, write_descriptors.size(),
      write_descriptors.data(), 0, nullptr);
  }
}

const CommandChain loader{
  {&load_world_vert_uniform_buffer, &unload_world_vert_uniform_buffer},
  {&load_world_frag_uniform_buffer, &unload_world_frag_uniform_buffer},
  {&load_descriptor_pool, &unload_descriptor_pool},
  // By destroying the pool the sets are also destroyed.
  {&load_descriptor_sets_world, nullptr},
  {&load_resources_to_descriptor_sets, nullptr}
};

}

namespace BluCat::GRA
{

Light::Light()
{
  loader.execute(this);
}

Light::~Light()
{
  loader.revert(this);
}

}
