/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "command_pool.hpp"

namespace BluCat::GRA
{

const CommandChain CommandPool::loader{
  {&CommandPool::load_command_pool, &CommandPool::unload_command_pool},
  {&CommandPool::load_command_buffers, nullptr}
};

CommandPool::CommandPool(QueueFamily *queue_family, uint32_t buffers_quantity):
  queue_family{queue_family}
{
  this->command_buffers.resize(buffers_quantity);

  CommandPool::loader.execute(this);
}

CommandPool::~CommandPool()
{
  CommandPool::loader.revert(this);
}

void
CommandPool::load_command_pool(void *obj)
{
  auto *self = static_cast<CommandPool*>(obj);

  VkCommandPoolCreateInfo command_pool_create_info;

  command_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  command_pool_create_info.pNext = nullptr;
  command_pool_create_info.flags =
      VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  command_pool_create_info.queueFamilyIndex = self->queue_family->family_index;

  if(vkCreateCommandPool(
       self->queue_family->device->device, &command_pool_create_info,
       nullptr, &self->command_pool) != VK_SUCCESS)
    throw CommandError{"Vulkan command pool could not be created."};
}

void
CommandPool::unload_command_pool(void *obj)
{
   auto *self = static_cast<CommandPool*>(obj);

  vkDestroyCommandPool(
    self->queue_family->device->device, self->command_pool, nullptr);
}

void
CommandPool::load_command_buffers(void *obj)
{
  auto *self = static_cast<CommandPool*>(obj);

  VkCommandBufferAllocateInfo command_buffer_info;
  command_buffer_info.sType =
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  command_buffer_info.pNext = nullptr;
  command_buffer_info.commandPool = self->command_pool;
  command_buffer_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  command_buffer_info.commandBufferCount = self->command_buffers.size();

  if(vkAllocateCommandBuffers(
       self->queue_family->device->device,
       &command_buffer_info, self->command_buffers.data()) != VK_SUCCESS)
    throw CommandError{"Vulkan command buffers could not be allocated."};
}

}
