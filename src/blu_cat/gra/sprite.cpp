/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sprite.hpp"

#include <array>

#include "../int/core.hpp"
#include "sprite.hpp"
#include "uniform_data_object.hpp"

namespace
{

struct SpriteBuilder
{
  BluCat::GRA::Sprite *sprite;
  const glm::vec4 &rect;

  SpriteBuilder(BluCat::GRA::Sprite *sprite, const glm::vec4 &rect);
};

SpriteBuilder::SpriteBuilder(BluCat::GRA::Sprite *sprite, const glm::vec4 &rect):
  sprite{sprite},
  rect{rect}
{
}

void
load_mesh(void *obj)
{
  auto self = static_cast<SpriteBuilder*>(obj);

  self->sprite->queue_family =
    BluCat::INT::core.vk_device_with_swapchain->get_queue_family_with_graphics();

  glm::vec2 rect[BluCat::GRA::Sprite::vertex_count]{
    glm::vec2{self->rect.x, self->rect.y},
    glm::vec2{self->rect.x, self->rect.w},
    glm::vec2{self->rect.z, self->rect.y},
    glm::vec2{self->rect.z, self->rect.w}
  };

  void *vertexes_data{&rect};
  static const size_t vertexes_size =
    sizeof(glm::vec2) * BluCat::GRA::Sprite::vertex_count;
  self->sprite->source_buffer = new BluCat::GRA::SourceBuffer{
    self->sprite->queue_family->device, vertexes_data, vertexes_size};
  self->sprite->vertex_buffer = new BluCat::GRA::DestinationBuffer{
    self->sprite->queue_family, self->sprite->source_buffer,
    VK_BUFFER_USAGE_VERTEX_BUFFER_BIT};
}

void
unload_mesh(void *obj)
{
  auto self = static_cast<SpriteBuilder*>(obj);

  delete self->sprite->vertex_buffer;
  delete self->sprite->source_buffer;
}

static const CommandChain loader{
  {&load_mesh, &unload_mesh},
};

}

namespace BluCat::GRA
{

Sprite::Sprite(std::shared_ptr<Texture> texture, const glm::vec4 &rect):
  texture{texture}
{
  SpriteBuilder sprite_builder(this, rect);
  loader.execute(&sprite_builder);
}

Sprite::~Sprite()
{
  glm::vec4 vector_4d{};
  SpriteBuilder sprite_builder(this, vector_4d);
  loader.revert(&sprite_builder);
}

}
