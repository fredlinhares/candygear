/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "queue.hpp"

#include "queue_family.hpp"

namespace BluCat::GRA
{

Queue::Queue(
  BluCat::GRA::QueueFamily *queue_family, VkQueue queue, int queue_index):
  queue_family{queue_family},
  queue{queue},
  queue_index{queue_index}
{
  this->queue_family->queue_states[this->queue_index].busy = true;
}

Queue::Queue(Queue &&that):
  queue{that.queue},
  queue_family{that.queue_family},
  queue_index{that.queue_index}
{
  that.queue_family = nullptr;
}

Queue& Queue::operator=(Queue &&that)
{
  this->queue = that.queue;
  this->queue_family = that.queue_family;
  this->queue_index = that.queue_index;

  that.queue_family = nullptr;

  return *this;
}

Queue::~Queue()
{
  if(this->queue_family)
  {
    std::unique_lock<std::mutex> lock{this->queue_family->queue_mutex};
    this->queue_family->queue_states[this->queue_index].busy = false;
  }
}

}
