/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_CHARACTER_H
#define BLU_CAT_GRA_CHARACTER_H 1

#include <ft2build.h>
#include FT_FREETYPE_H

#include "vulkan.hpp"

#include <vector>

namespace BluCat::GRA
{

struct Character
{
  VkImage image;
  VkDeviceMemory device_memory;
  int32_t bearing_y, bearing_x;
  uint32_t width, height, advance, mip_levels;

  Character(FT_Face face, uint32_t character_code);
  ~Character();

  Character(Character const&) = delete;
  Character& operator=(Character const&) = delete;

  static std::vector<uint32_t>
  str_to_unicode(const char* str);
};

}

#endif /* BLU_CAT_GRA_CHARACTER_H */
