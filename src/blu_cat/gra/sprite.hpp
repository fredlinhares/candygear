/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SPRITE_H
#define BLU_CAT_GRA_SPRITE_H 1

#include <memory>
#include <unordered_map>
#include <vector>

#include "vulkan.hpp"
#include "destination_buffer.hpp"
#include "queue_family.hpp"
#include "uniform_buffer.hpp"
#include "texture.hpp"

namespace BluCat::GRA
{

struct Sprite
{
  static const uint32_t vertex_count{4};

  QueueFamily *queue_family;

  SourceBuffer *source_buffer;
  DestinationBuffer *vertex_buffer;

  std::shared_ptr<Texture> texture;

  Sprite(std::shared_ptr<Texture> texture, const glm::vec4 &rect);
  ~Sprite();
};

}

#endif /* BLU_CAT_GRA_SPRITE_H */
