/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_QUEUE_H
#define BLU_CAT_GRA_QUEUE_H 1

#include "vulkan.hpp"

namespace BluCat::GRA
{
class QueueFamily;

struct Queue
{
  friend class BluCat::GRA::QueueFamily;

  Queue(const Queue &t) = delete;
  Queue& operator=(const Queue &t) = delete;

  VkQueue queue;

  template<typename T>
  void submit_one_time_command(
    const VkCommandBuffer vk_command_buffer, T commands);

  Queue(Queue &&that);
  Queue& operator=(Queue &&that);

  ~Queue();

private:
  BluCat::GRA::QueueFamily *queue_family;
  int queue_index;

  Queue(BluCat::GRA::QueueFamily *queue_family, VkQueue queue, int queue_index);
};

template<typename T> void
Queue::submit_one_time_command(
  const VkCommandBuffer vk_command_buffer, T commands)
{
  VkCommandBufferBeginInfo buffer_begin_info{};
  buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(vk_command_buffer, &buffer_begin_info);

  commands();

  vkEndCommandBuffer(vk_command_buffer);

  VkSubmitInfo submit_info{};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.pNext = nullptr;
  submit_info.waitSemaphoreCount = 0;
  submit_info.pWaitSemaphores = nullptr;
  submit_info.pWaitDstStageMask = nullptr;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &vk_command_buffer;
  submit_info.signalSemaphoreCount = 0;
  submit_info.pSignalSemaphores = nullptr;

  vkQueueSubmit(this->queue, 1, &submit_info, VK_NULL_HANDLE);
  vkQueueWaitIdle(this->queue);
}

}

#endif /* BLU_CAT_GRA_QUEUE_H */
