/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_FRAME_H
#define BLU_CAT_GRA_FRAME_H 1

#include <vector>

#include "../vulkan.hpp"

namespace BluCat::GRA
{

template<typename T>
struct Frame
{
  const T value;
  const float timestamp;

  Frame(T value, float timestamp):
  value{value},
  timestamp{timestamp}
  {
  }

};

template<typename T>
struct Channel
{
  int current_index{0};
  std::vector<Frame<T>> key_frames;

  inline glm::mat4
  interpolate(
    float animation_time,
    glm::mat4 (*single_frame)(T frame),
    glm::mat4 (*multiple_frames)(T current_frame, T next_frame, float scale))
  {
    if(this->key_frames.size() == 1)
      return single_frame(this->key_frames[0].value);
    else
    {
      while(animation_time > this->key_frames[current_index].timestamp)
        this->current_index++;

      float scale_factor;
      Frame<T> *previous_frame;
      Frame<T> *next_frame{&(this->key_frames[this->current_index])};
      if(this->current_index == 0)
      {
	previous_frame = &(this->key_frames[this->key_frames.size() - 1]);
        float midway_length{animation_time - 0};
        float frames_diff{next_frame->timestamp - 0};
        scale_factor = midway_length / frames_diff;
      }
      else
      {
	previous_frame = &(this->key_frames[this->current_index - 1]);
        float midway_length{animation_time - previous_frame->timestamp};
        float frames_diff{next_frame->timestamp - previous_frame->timestamp};
        scale_factor = midway_length / frames_diff;
      }

      return multiple_frames(
	previous_frame->value, next_frame->value, scale_factor);
    }
  };
};

}
#endif /* BLU_CAT_GRA_FRAME_H */
