/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graphics_pipeline_2d_wired_layout.hpp"

#include <array>

#include "../int/core.hpp"
#include "graphics_pipeline_2d_solid_layout.hpp"
#include "uniform_data_object.hpp"

namespace
{

void
load_pipeline(void *obj)
{
  auto self = static_cast<BluCat::GRA::GraphicsPipeline2DWiredLayout*>(obj);

  std::array<VkDescriptorSetLayout, 1> set_layouts{
    BluCat::INT::core.vk_descriptor_set_layout->view
  };

  std::array<VkPushConstantRange, 2> push_constants;
  push_constants[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
  push_constants[0].offset = 0;
  push_constants[0].size = sizeof(BluCat::GRA::UDOVector4D);

  push_constants[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
  push_constants[1].offset = sizeof(BluCat::GRA::UDOVector4D);
  push_constants[1].size = sizeof(BluCat::GRA::UDOVector3D);

  VkPipelineLayoutCreateInfo pipeline_layout_info{};
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = set_layouts.size();
  pipeline_layout_info.pSetLayouts = set_layouts.data();
  pipeline_layout_info.pushConstantRangeCount = push_constants.size();
  pipeline_layout_info.pPushConstantRanges = push_constants.data();

  if(vkCreatePipelineLayout(
       BluCat::INT::core.vk_device_with_swapchain->device, &pipeline_layout_info,
       nullptr, &self->pipeline) != VK_SUCCESS)
    throw CommandError{"Failed to create Vulkan pipeline layout."};
}

void
unload_pipeline(void *obj)
{
  auto self = static_cast<BluCat::GRA::GraphicsPipeline2DWiredLayout*>(obj);

  vkDestroyPipelineLayout(
    BluCat::INT::core.vk_device_with_swapchain->device, self->pipeline, nullptr);
}

const CommandChain loader{
  {&load_pipeline, &unload_pipeline}
};

}

namespace BluCat::GRA
{

GraphicsPipeline2DWiredLayout::GraphicsPipeline2DWiredLayout()
{
  loader.execute(this);
}

GraphicsPipeline2DWiredLayout::~GraphicsPipeline2DWiredLayout()
{
  loader.revert(this);
}

}
