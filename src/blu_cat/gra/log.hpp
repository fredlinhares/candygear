/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CANDY_GEAR_LOG_H
#define CANDY_GEAR_LOG_H 1

#include <string>

#define CANDY_GEAR_LOG_LEVEL_FATAL 0
#define CANDY_GEAR_LOG_LEVEL_ERROR 1
#define CANDY_GEAR_LOG_LEVEL_WARNING 2
#define CANDY_GEAR_LOG_LEVEL_INFORMATION 3
#define CANDY_GEAR_LOG_LEVEL_DEBUG 4
#define CANDY_GEAR_LOG_LEVEL_TRACE 5

namespace Log
{

enum class Level
{
  Fatal = CANDY_GEAR_LOG_LEVEL_FATAL,
  Error = CANDY_GEAR_LOG_LEVEL_ERROR,
  Warning = CANDY_GEAR_LOG_LEVEL_WARNING,
  Information = CANDY_GEAR_LOG_LEVEL_INFORMATION,
  Debug = CANDY_GEAR_LOG_LEVEL_DEBUG,
  Trace = CANDY_GEAR_LOG_LEVEL_TRACE
};

struct Logger
{

  void
  message(Level lvl, const char* text);

  inline void
  message(Level lvl, const std::string &text) {message(lvl, text.c_str());}
};

}

#endif /* CANDY_GEAR_LOG_H */
