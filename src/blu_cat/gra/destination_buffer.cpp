/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "destination_buffer.hpp"

#include "command_pool.hpp"

namespace BluCat::GRA
{

DestinationBuffer::DestinationBuffer(
  QueueFamily *queue_family, SourceBuffer *source_buffer,
  VkBufferUsageFlags buffer_usage)
{
  this->device = queue_family->device;
  this->device_size = source_buffer->device_size;
  this->buffer_usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | buffer_usage;
  this->memory_properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
  this->queue_family = queue_family;
  this->source_buffer = source_buffer;

  BaseBuffer::loader.execute(dynamic_cast<BaseBuffer*>(this));

  this->copy_data();
}

DestinationBuffer::DestinationBuffer(
  DestinationBuffer &&that)
{
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;
}

DestinationBuffer&
DestinationBuffer::operator=(DestinationBuffer &&that)
{
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;

  return *this;
}

DestinationBuffer::~DestinationBuffer()
{
  BaseBuffer::loader.revert(dynamic_cast<BaseBuffer*>(this));
}

void
DestinationBuffer::copy_data()
{
  CommandPool command_pool(this->queue_family, 1);
  Queue transfer_queue{this->queue_family->get_queue()};
  this->device_size = source_buffer->device_size;

  this->vk_command_buffer = command_pool.command_buffers[0];

  VkCommandBufferBeginInfo begin_info = {};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  VkBufferCopy copy_region = {};
  copy_region.srcOffset = 0;
  copy_region.dstOffset = 0;
  copy_region.size = this->device_size;

  vkBeginCommandBuffer(this->vk_command_buffer, &begin_info);

  vkCmdCopyBuffer(
    this->vk_command_buffer, this->source_buffer->buffer, this->buffer, 1,
    &copy_region);

  vkEndCommandBuffer(this->vk_command_buffer);

  VkSubmitInfo submit_info = {};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &this->vk_command_buffer;

  vkQueueSubmit(transfer_queue.queue, 1, &submit_info, VK_NULL_HANDLE);

  vkQueueWaitIdle(transfer_queue.queue);
}

}
