/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_VIEW_3D_H
#define BLU_CAT_GRA_VIEW_3D_H 1

#include "view_2d.hpp"

namespace BluCat::GRA
{

struct View3D: public View2D
{
  float field_of_view;
  // FIXME: if this vector get resized, it can cause a segmentation fault!
  std::vector<UniformBuffer> ub_3d;

  std::vector<VkDescriptorSet> descriptor_sets_3d;

  std::shared_ptr<glm::vec3> camera_position;
  std::shared_ptr<glm::quat> camera_orientation;

  View3D(glm::vec4 region, float projection_width, float projection_height);
  ~View3D();

  void
  load_descriptor_sets(VkDescriptorPool descriptor_pool);

  void
  unload_descriptor_sets();
};

}

#endif /* BLU_CAT_GRA_VIEW_3D_H */
