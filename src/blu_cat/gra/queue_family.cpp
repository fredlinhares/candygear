/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "queue_family.hpp"

#ifdef DEBUG
#include <sstream>
#endif

#include "../int/core.hpp"

namespace BluCat::GRA
{

QueueFamily::QueueFamily(
  BluCat::GRA::Device *device, uint32_t family_index,
  const VkQueueFamilyProperties &queue_family_properties):
  queue_mutex{}
{

#ifdef DEBUG
  std::stringstream message{};
  message << "Queue quantity: " << queue_family_properties.queueCount <<
    std::endl;
  message << "Graphics: " <<
    (queue_family_properties.queueFlags & VK_QUEUE_GRAPHICS_BIT ?
     "true" : "false") << std::endl;
  message << "Compute: " <<
    (queue_family_properties.queueFlags & VK_QUEUE_COMPUTE_BIT ?
     "true" : "false") << std::endl;
  message << "Transfer: " <<
    (queue_family_properties.queueFlags & VK_QUEUE_TRANSFER_BIT ?
     "true" : "false") << std::endl;
  message << "Sparse Binding: " <<
    (queue_family_properties.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT ?
     "true" : "false") << std::endl;
  INT::core.log.message(Log::Level::Trace, message.str());
#endif

  this->device = device;
  this->family_index = family_index;
  this->family_properties = queue_family_properties;

  // Create queues
  {
    auto queue_count = this->family_properties.queueCount;
    this->queue_states.resize(queue_count);

    for(auto i{0}; i < queue_count; i++)
    {
      vkGetDeviceQueue(device->device, this->family_index, i,
		       &this->queue_states[i].queue);
      if(this->queue_states[i].queue == VK_NULL_HANDLE)
	throw std::runtime_error("Failed to get Vulkan queue.");
    }
  }
}

Queue
QueueFamily::get_queue()
{
  std::unique_lock<std::mutex> lock{this->queue_mutex};

  for(auto i{0}; i < this->queue_states.size(); i++)
    if(!this->queue_states[i].busy)
      return Queue(this, this->queue_states[i].queue, i);

  throw std::length_error("No free queues found.");
}

}
