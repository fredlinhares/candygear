/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_ANIMATION_H
#define BLU_CAT_GRA_ANIMATION_H 1

#include <vector>

#include "vulkan.hpp"
#include "animation/frame.hpp"

namespace BluCat::GRA
{

struct Bone
{
  glm::mat4x4 offset_matrix;

  Bone(glm::mat4 offset_matrix);
};

struct BoneTransform
{
  uint32_t bone_id;
  Channel<glm::vec3> positions;
  Channel<glm::quat> rotations;
  Channel<glm::vec3> scales;
};

struct Animation
{
  std::vector<BoneTransform> bone_transforms;
  float final_time;
};

}

#endif /* BLU_CAT_GRA_ANIMATION_H */
