/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "uniform_buffer.hpp"

#include <cstring>
#include <stdexcept>

namespace BluCat::GRA
{

UniformBuffer::UniformBuffer(Device *device, VkDeviceSize data_size)
{
  this->device = device;
  this->device_size = data_size;
  this->buffer_usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
  this->memory_properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
    VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  try
  {
    BaseBuffer::loader.execute(static_cast<BaseBuffer*>(this));
  }
  catch(const CommandError &command_error)
  {
    std::string error{"Could not initialize Vulkan uniform buffer → "};
    error += command_error.what();
    throw CommandError{error};
  }
}

UniformBuffer::~UniformBuffer()
{
  BaseBuffer::loader.revert(static_cast<BaseBuffer*>(this));
}

UniformBuffer::UniformBuffer(UniformBuffer &&that)
{
  this->device = that.device;
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;
}

UniformBuffer&
UniformBuffer::operator=(UniformBuffer &&that)
{
  this->device = that.device;
  this->buffer = that.buffer;
  this->device_memory = that.device_memory;
  this->device_size = that.device_size;
  this->buffer_usage = that.buffer_usage;
  this->memory_properties = that.memory_properties;

  that.buffer = VK_NULL_HANDLE;
  that.device_memory = VK_NULL_HANDLE;

  return *this;
}

void
UniformBuffer::copy_data(void *ubo)
{
  void *data;
  vkMapMemory(this->device->device, this->device_memory, 0,
              this->device_size, 0, &data);
  memcpy(data, ubo, this->device_size);
  vkUnmapMemory(this->device->device, this->device_memory);
}

}
