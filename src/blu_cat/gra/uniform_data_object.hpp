/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_UNIFORM_DATA_OBJECT_H
#define BLU_CAT_GRA_UNIFORM_DATA_OBJECT_H 1

#include "vulkan.hpp"
#include "skeletal_mesh_vertex.hpp"

namespace BluCat::GRA
{

// UDO = "uniform data object"

struct UDOView2D
{
  glm::mat4 proj;
};

struct UDOView3D
{
  glm::mat4 view;
  glm::mat4 proj;
};

struct UDOWorld3D_Vert
{
  glm::vec4 ambient_light_color;
};

struct UDOWorld3D_Frag
{
  glm::vec3 directional_light_direction;
  glm::vec4 directional_light_color;
};

struct UDOStaticModel
{
  glm::mat4 base_matrix;
};

struct UDOSkeletalModel
{
  glm::mat4 base_matrix;
  glm::mat4 bone_matrices[SKELETAL_MESH_MAX_NUM_OF_BONES];
};

struct UDOVector4D
{
  glm::vec4 vector;
};

struct UDOVector3D
{
  glm::vec3 vectors;
};

struct UDOSprite3D
{
  glm::vec3 position;
  uint32_t padding;
  glm::vec2 size;
};

}

#endif /* BLU_CAT_GRA_UNIFORM_DATA_OBJECT_H */
