/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "static_mesh.hpp"

#include "binary_reader.hpp"
#include "../com/command.hpp"
#include "../int/core.hpp"
#include "static_mesh_vertex.hpp"

namespace
{

// Data that is only needed for the command chain but not for the StaticMesh
// goes here.
struct MeshBuilder
{
  std::string mesh_path;
  BluCat::GRA::StaticMesh *mesh;

  MeshBuilder(BluCat::GRA::StaticMesh *m, std::string mp);
  MeshBuilder(BluCat::GRA::StaticMesh *m, const char* mp);
};

MeshBuilder::MeshBuilder(BluCat::GRA::StaticMesh *m, std::string mp):
  mesh{m},
  mesh_path{mp}
{
}

MeshBuilder::MeshBuilder(BluCat::GRA::StaticMesh *m, const char *mp):
  MeshBuilder{m, std::string(mp)}
{
}

void
load_mesh(void *obj)
{
  auto self = static_cast<MeshBuilder*>(obj);

  BinaryReader input{self->mesh_path};

  self->mesh->queue_family =
    BluCat::INT::core.vk_device_with_swapchain->get_queue_family_with_graphics();

  { // Load vertexes.
    auto vertex_count{input.read_ui32()};
    std::vector<BluCat::GRA::StaticMeshVertex> vertexes{vertex_count};

    for(auto i{0}; i < vertex_count; i++)
    {
      vertexes[i].position = input.read_vec3();
      vertexes[i].normal = input.read_vec3();
      vertexes[i].texture_coord = input.read_vec2();
    }

    void *vertexes_data{vertexes.data()};
    size_t vertexes_size = sizeof(vertexes[0]) * vertexes.size();
    self->mesh->source_vertex_buffer = new BluCat::GRA::SourceBuffer{
      self->mesh->queue_family->device, vertexes_data, vertexes_size};
    self->mesh->vertex_buffer = new BluCat::GRA::DestinationBuffer{
      self->mesh->queue_family, self->mesh->source_vertex_buffer,
      VK_BUFFER_USAGE_VERTEX_BUFFER_BIT};
  }

  { // Load indexes
    self->mesh->index_count = input.read_ui32();
    std::vector<uint32_t> indexes(self->mesh->index_count);

    for(auto i{0}; i < self->mesh->index_count; i++)
      indexes[i] = input.read_ui32();

    void *indexes_data{indexes.data()};
    size_t indexes_size{sizeof(indexes[0]) * indexes.size()};
    BluCat::GRA::SourceBuffer source_index_buffer{
      self->mesh->queue_family->device, indexes_data, indexes_size};
    self->mesh->index_buffer = new BluCat::GRA::DestinationBuffer{
      self->mesh->queue_family, &source_index_buffer,
      VK_BUFFER_USAGE_INDEX_BUFFER_BIT};
  }
}

void
unload_mesh(void *obj)
{
  auto self = static_cast<MeshBuilder*>(obj);

  delete self->mesh->index_buffer;
  delete self->mesh->vertex_buffer;
  delete self->mesh->source_vertex_buffer;
}

static const CommandChain loader{
  {&load_mesh, &unload_mesh}
};

}

namespace BluCat::GRA
{

StaticMesh::StaticMesh(std::string mesh_path)
{
  MeshBuilder mesh_builder(this, mesh_path);
  loader.execute(&mesh_builder);
}

StaticMesh::StaticMesh(const char* mesh_path):
  StaticMesh{std::string(mesh_path)}
{
}

StaticMesh::~StaticMesh()
{
  MeshBuilder mesh_builder(this, "");
  loader.revert(&mesh_builder);
}

}
