/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "view_3d.hpp"

#include <array>

#include "../int/core.hpp"
#include "uniform_data_object.hpp"

namespace
{

void
load_3d_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::View3D*>(obj);

  try
  {
    self->ub_3d.reserve(BluCat::INT::core.vk_swapchain->images_count);
    for(auto i{0}; i < BluCat::INT::core.vk_swapchain->images_count; i++)
      self->ub_3d.emplace_back(
	BluCat::INT::core.vk_device_with_swapchain, sizeof(BluCat::GRA::UDOView3D));
  }
  catch(const std::exception& e)
  {
    throw CommandError{e.what()};
  }
}

void
unload_3d_uniform_buffer(void *obj)
{
  auto self = static_cast<BluCat::GRA::View3D*>(obj);

  self->ub_3d.clear();
}

void
load_descriptor_sets_3d(void *obj)
{
  auto self = static_cast<BluCat::GRA::View3D*>(obj);

  std::vector<VkDescriptorSetLayout> layouts(
    BluCat::INT::core.vk_swapchain->images_count,
    BluCat::INT::core.vk_descriptor_set_layout->view);

  VkDescriptorSetAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = self->descriptor_pool;
  alloc_info.descriptorSetCount = layouts.size();
  alloc_info.pSetLayouts = layouts.data();

  self->descriptor_sets_3d.resize(layouts.size());
  if(vkAllocateDescriptorSets(
       BluCat::INT::core.vk_device_with_swapchain->device, &alloc_info,
       self->descriptor_sets_3d.data()) != VK_SUCCESS)
    throw CommandError{"Failed to create Vulkan descriptor sets for view."};
}

void
load_resources_to_descriptor_sets_3d(void *obj)
{
  auto self = static_cast<BluCat::GRA::View3D*>(obj);

  for(auto i{0}; i < self->ub_3d.size(); i++)
  {
    VkDescriptorBufferInfo view_3d_info{};
    view_3d_info.buffer = self->ub_3d[i].buffer;
    view_3d_info.offset = 0;
    view_3d_info.range = sizeof(BluCat::GRA::UDOView3D);

    std::array<VkWriteDescriptorSet, 1> write_descriptors{};
    write_descriptors[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptors[0].dstSet = self->descriptor_sets_3d[i];
    write_descriptors[0].dstBinding = 0;
    write_descriptors[0].dstArrayElement = 0;
    write_descriptors[0].descriptorCount = 1;
    write_descriptors[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptors[0].pBufferInfo = &view_3d_info;
    write_descriptors[0].pImageInfo = nullptr;
    write_descriptors[0].pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(
      BluCat::INT::core.vk_device_with_swapchain->device, write_descriptors.size(),
      write_descriptors.data(), 0, nullptr);
  }
}

const CommandChain loader{
  {&load_3d_uniform_buffer, &unload_3d_uniform_buffer}
};

const CommandChain descriptor_sets_loader{
  {&load_descriptor_sets_3d, nullptr},
  {&load_resources_to_descriptor_sets_3d, nullptr}
};

}

namespace BluCat::GRA
{

View3D::View3D(
  glm::vec4 region, float projection_width, float projection_height):
  View2D{region, projection_width, projection_height},
  field_of_view{45.0f},
  camera_position{std::make_shared<glm::vec3>(0.0f, 0.0f, 0.0f)},
  camera_orientation{std::make_shared<glm::quat>(0.0f, 0.0f, 0.0f, 0.0f)}
{
  ::loader.execute(this);
}

View3D::~View3D()
{
  ::loader.revert(this);
}

void
View3D::load_descriptor_sets(VkDescriptorPool descriptor_pool)
{
  if(this->descriptor_pool != VK_NULL_HANDLE) return;

  auto parent = dynamic_cast<BluCat::GRA::View2D*>(this);
  this->descriptor_pool = descriptor_pool;
  View2D::descriptor_sets_loader.execute(parent);
  ::descriptor_sets_loader.execute(this);
}

void
View3D::unload_descriptor_sets()
{
  if(this->descriptor_pool == VK_NULL_HANDLE) return;

  auto parent = dynamic_cast<BluCat::GRA::View2D*>(this);
  this->descriptor_pool = VK_NULL_HANDLE;
  ::descriptor_sets_loader.revert(this);
  View2D::descriptor_sets_loader.revert(parent);
}

}
