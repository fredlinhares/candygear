/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_COMMAND_POOL_H
#define BLU_CAT_GRA_COMMAND_POOL_H 1

#include <vector>

#include "../com/command.hpp"
#include "device.hpp"
#include "vulkan.hpp"

namespace BluCat::GRA
{

class CommandPool
{
  CommandPool(const CommandPool &t) = delete;
  CommandPool& operator=(const CommandPool &t) = delete;
  CommandPool(const CommandPool &&t) = delete;
  CommandPool& operator=(const CommandPool &&t) = delete;

public:
  std::vector<VkCommandBuffer> command_buffers;

  CommandPool(QueueFamily *queue_family, uint32_t buffers_quantity);
  ~CommandPool();

private:
  static const CommandChain loader;

  QueueFamily *queue_family;
  VkCommandPool command_pool;

  static void
  load_command_pool(void *obj);
  static void
  unload_command_pool(void *obj);

  static void
  load_command_buffers(void *obj);
};

}

#endif /* BLU_CAT_GRA_COMMAND_POOL_H */
