/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SOURCE_BUFFER_H
#define BLU_CAT_GRA_SOURCE_BUFFER_H 1

#include "base_buffer.hpp"

namespace BluCat::GRA
{

struct SourceBuffer: public BaseBuffer
{
  SourceBuffer(const SourceBuffer &t) = delete;
  SourceBuffer&
  operator=(const SourceBuffer &t) = delete;

  SourceBuffer(Device *device, void *data, size_t data_size);

  SourceBuffer(SourceBuffer &&that);
  SourceBuffer&
  operator=(SourceBuffer &&that);

  ~SourceBuffer();

  void
  copy_data(void *src_data);
};

}

#endif /* BLU_CAT_GRA_SOURCE_BUFFER_H */
