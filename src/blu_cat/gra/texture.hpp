/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_TEXTURE_H
#define BLU_CAT_GRA_TEXTURE_H 1

#include <string>

#include "vulkan.hpp"
#include "font.hpp"
#include "queue_family.hpp"

namespace BluCat::GRA
{

struct Texture
{
  QueueFamily *queue_family;

  VkImage image;
  VkSampler sampler;
  VkImageView view;
  VkDeviceMemory device_memory;
  uint32_t width, height;
  uint32_t mip_levels;

  VkDescriptorPool descriptor_pool;
  std::vector<VkDescriptorSet> descriptor_sets;

  Texture(Font *font, const char *str);
  Texture(const std::string &texture_path);
  Texture(const char* texture_path);
  ~Texture();
};

}

#endif /* BLU_CAT_GRA_TEXTURE_H */
