/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "log.hpp"

#include <iostream>

namespace Log
{

void
Logger::message(Level lvl, const char* text)
{
  std::cout << "[";
  switch(lvl)
  {
  case Level::Fatal:
    std::cout << "\e[1;35mFatal\e[0;0m";
    break;
  case Level::Error:
    std::cout << "\e[1;31mError\e[0;0m";
    break;
  case Level::Warning:
    std::cout << "\e[1;33mWarning\e[0;0m";
    break;
  case Level::Information:
    std::cout << "\e[1;32mInformation\e[0;0m";
    break;
  case Level::Debug:
    std::cout << "\e[1;36mDebug\e[0;0m";
    break;
  case Level::Trace:
    std::cout << "\e[1;34mTrace\e[0;0m";
    break;
  }
  std::cout << "] ";
  std::cout << text << std::endl;
}

}
