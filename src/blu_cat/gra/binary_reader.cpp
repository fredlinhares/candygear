/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "binary_reader.hpp"

#include <fstream>

namespace
{

union IntAndFloat32bit{
  uint32_t i;
  float f;
};

union IntAndFloat64bit{
  uint64_t i;
  double f;
};

}

BinaryReader::BinaryReader(std::string file_path):
  _pointer{0}
{
  std::ifstream file(file_path, std::ios::binary | std::ios::ate);
	if(!file.is_open())
	{
		std::string error{"failed to open file: "};
		error += file_path;
		throw std::runtime_error{error};
	}

  this->_size = file.tellg();
  file.seekg(0);
  this->data = new uint8_t[this->_size];
  file.read((char*)data, this->_size);
}

BinaryReader::BinaryReader(const char *file_path):
  BinaryReader{std::string(file_path)}
{
}

BinaryReader::~BinaryReader()
{
  delete[] this->data;
}

uint8_t
BinaryReader::read_ui8()
{
  return this->data[this->_pointer++];
}

uint32_t
BinaryReader::read_ui32()
{
  uint8_t b1{this->data[_pointer++]}, b2{this->data[_pointer++]},
    b3{this->data[_pointer++]}, b4{this->data[_pointer++]};

  return b1 << 24 | b2 << 16 | b3 << 8 | b4;
}

uint64_t
BinaryReader::read_ui64()
{
  uint8_t b1{this->data[_pointer++]}, b2{this->data[_pointer++]},
    b3{this->data[_pointer++]}, b4{this->data[_pointer++]},
    b5{this->data[_pointer++]}, b6{this->data[_pointer++]},
    b7{this->data[_pointer++]}, b8{this->data[_pointer++]};

  return (uint64_t)b1 << 56 | (uint64_t)b2 << 48 | (uint64_t)b3 << 40 |
    (uint64_t)b4 << 32 | (uint64_t)b5 << 24 | (uint64_t)b6 << 16 |
    (uint64_t)b7 << 8 | (uint64_t)b8;
}

float
BinaryReader::read_float()
{
  IntAndFloat32bit num;
  num.i = read_ui32();

  return num.f;
}

double
BinaryReader::read_double()
{
  IntAndFloat64bit num;
  num.i = read_ui64();

  return num.f;
}

glm::vec2
BinaryReader::read_vec2()
{
  IntAndFloat32bit x{read_ui32()}, y{read_ui32()};

  return glm::vec2{x.f, y.f};
}

glm::vec3
BinaryReader::read_vec3()
{
  IntAndFloat32bit x{read_ui32()}, y{read_ui32()}, z{read_ui32()};

  return glm::vec3{x.f, y.f, z.f};
}

glm::quat
BinaryReader::read_quat()
{
  IntAndFloat32bit w{read_ui32()}, x{read_ui32()}, y{read_ui32()},
    z{read_ui32()};

  return glm::quat{w.f, x.f, y.f, z.f};
}

glm::mat4
BinaryReader::read_mat4()
{
  glm::mat4 matrix;
  float *offset_matrix_data{glm::value_ptr(matrix)};

  for(int i{0}; i < 16; i++)
  {
    IntAndFloat32bit num;
    num.i = read_ui32();
    offset_matrix_data[i] = num.f;
  }

  return matrix;
}

void
BinaryReader::read_chars(char *str, int size)
{
  for(int i{0}; i < size; i++)
    str[i] = (char)data[this->_pointer++];
}

