/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_BASE_BUFFER_H
#define BLU_CAT_GRA_BASE_BUFFER_H 1

#include "../com/command.hpp"
#include "vulkan.hpp"
#include "device.hpp"

namespace BluCat::GRA
{

class BaseBuffer
{
 public:
  virtual ~BaseBuffer(){};

  VkBuffer buffer;
  VkDeviceMemory device_memory;
  VkDeviceSize device_size;
  VkBufferUsageFlags buffer_usage;
  VkMemoryPropertyFlags memory_properties;

 protected:
  static const CommandChain loader;

  Device *device;

  static void
  load_buffer(void *obj);
  static void
  unload_buffer(void *obj);

  static void
  load_memory(void *obj);
  static void
  unload_memory(void *obj);
};

}

#endif /* BLU_CAT_GRA_BASE_BUFFER_H */
