/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_GRA_SPRITE_3D_H
#define BLU_CAT_GRA_SPRITE_3D_H 1

#include "vulkan.hpp"
#include "sprite.hpp"

namespace BluCat::GRA
{

struct Sprite3D
{
  std::shared_ptr<Sprite> sprite;
  std::shared_ptr<glm::vec3> position;
  glm::vec2 size;

  QueueFamily *queue_family;

  std::vector<UniformBuffer> uniform_buffers;
  VkDescriptorPool descriptor_pool;
  std::vector<VkDescriptorSet> descriptor_sets;

  Sprite3D(
    std::shared_ptr<Sprite> sprite, std::shared_ptr<glm::vec3> position,
    glm::vec2 size);
  ~Sprite3D();
};

}

#endif /* BLU_CAT_GRA_SPRITE_3D_H */
