/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "font.hpp"

#include "../int/core.hpp"

namespace BluCat::GRA
{

Font::Font(const char* font_path, int font_size)
{
  FT_Error error;
  error = FT_New_Face(
		BluCat::INT::core.font_library, font_path, 0, &this->face);
  if(error == FT_Err_Unknown_File_Format) throw std::invalid_argument(
    "The font file could be opened and read, but it appears that its font "
    "format is unsupported.");
  else if(error) throw std::invalid_argument(
    "The font file could not be opened or read, or it is broken.");

  error = FT_Set_Pixel_Sizes(this->face, 0, font_size);
  if(error) throw std::invalid_argument("Failed to load font size.");
}

Font::~Font()
{
  FT_Done_Face(this->face);
}

std::shared_ptr<Character>
Font::character(uint32_t character_code)
{
  if(!this->characters.contains(character_code))
    this->characters.emplace(
      character_code, std::make_shared<Character>(this->face, character_code));

  return this->characters.at(character_code);
}

}
