/*
 * Copyright 2022-2025 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_SERVER_SERVER_H
#define BLU_CAT_SERVER_SERVER_H 1

#include "../common/connection.hpp"
#include "../common/connection_callback.hpp"
#include "../common/tsqueue.hpp"

namespace BluCat::NET
{

class Server: public ConnectionCallback
{
	std::mutex mut;
	asio::io_context io_context;
	std::thread thread_context;
	asio::ip::tcp::acceptor acceptor;
	MessageCallback *(*callback_instantiator)();

	TSQueue<unsigned long> free_connection_slots;
	std::vector<Connection*> connections;

	void
	wait_for_client_connections();

public:
	void
	read_messages();

	void
	end_connection(unsigned long index);

	Server(MessageCallback *(*callback_instantiator)(), const uint16_t port);
	~Server();
};

}

#endif /* BLU_CAT_SERVER_SERVER_H */
