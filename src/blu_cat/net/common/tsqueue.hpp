/*
 * Copyright 2022-2025 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_NET_COMMON_TSQUEUE_H
#define BLU_CAT_NET_COMMON_TSQUEUE_H 1

#include <deque>

namespace BluCat::NET
{

template<typename T>
class TSQueue
{
protected:
	std::mutex mut;
	std::deque<T> deq_queue;

public:
	TSQueue() = default;
	TSQueue(const TSQueue<T>&) = delete;

	const T&
	front()
	{
		std::scoped_lock lock(this->mut);
		return this->deq_queue.front();
	}

	const T&
	back()
	{
		std::scoped_lock lock(this->mut);
		return this->deq_queue.back();
	}

	void
	push_front(const T& item)
	{
		std::scoped_lock lock(this->mut);
		this->deq_queue.emplace_front(std::move(item));
	}

	void
	push_back(const T& item)
	{
		std::scoped_lock lock(this->mut);
		this->deq_queue.emplace_back(std::move(item));
	}

	size_t
	size()
	{
		std::scoped_lock lock(this->mut);
		return this->deq_queue.size();
	}

	void
	clear()
	{
		std::scoped_lock lock(this->mut);
		return this->deq_queue.clear();
	}

	T
	pop_front()
	{
		std::scoped_lock lock(this->mut);
		auto t = std::move(this->deq_queue.front());
		deq_queue.pop_front();
		return t;
	}

	T
	pop_back()
	{
		std::scoped_lock lock(this->mut);
		auto t = std::move(this->deq_queue.back());
		deq_queue.pop_back();
		return t;
	}

	virtual
	~TSQueue()
	{
		this->clear();
	}
};

}

#endif /* BLU_CAT_NET_COMMON_TSQUEUE_H */
