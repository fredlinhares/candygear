/*
 * Copyright 2022-2025 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_NET_COMMON_CONNECTION_H
#define BLU_CAT_NET_COMMON_CONNECTION_H 1

#include <iostream>

#define ASIO_STANDALONE
#include <asio.hpp>
#include <asio/ts/buffer.hpp>
#include <asio/ts/internet.hpp>

#include "connection_callback.hpp"
#include "message.hpp"
#include "message_callback.hpp"
#include "tsqueue.hpp"

namespace BluCat::NET
{

class Connection
{
protected:
  unsigned long index;
	asio::io_context &io_context;

	asio::ip::tcp::socket socket;
	TSQueue<Message> messages;

	Message reading_message;
	MessageCallback *message_callback;
	ConnectionCallback *connection_callback;

	void
	read_header();

	void
	read_body();

public:
	bool
	send(const uint32_t id, const std::vector<uint8_t> &msg);

	void
	read_messages();

	Connection(MessageCallback *message_callback,
						 ConnectionCallback *connection_callback,
						 asio::io_context &io_context, asio::ip::tcp::socket socket,
						 unsigned long index);

	virtual
	~Connection();
};

}

#endif /* BLU_CAT_NET_COMMON_CONNECTION_H */
