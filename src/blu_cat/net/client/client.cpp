/*
 * Copyright 2022-2025 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client.hpp"

namespace BluCat::NET
{

void
Client::read_messages()
{
	std::scoped_lock lock(this->mut);
	if(this->connection) this->connection->read_messages();
}

void
Client::end_connection(unsigned long index)
{
	std::scoped_lock lock(this->mut);

	delete this->connection;
	this->connection = nullptr;
}

Client::Client(
	MessageCallback *(*callback_instantiator)(), const char *host,
	const uint16_t port):
	callback_instantiator{callback_instantiator}
{
	asio::error_code error;
	asio::ip::tcp::endpoint endpoint{asio::ip::make_address(host, error), port};

	asio::ip::tcp::socket socket(this->io_context);
	socket.connect(endpoint);

	this->connection = new Connection(
		this->callback_instantiator(), this, this->io_context, std::move(socket),
		0);

	this->thread_context = std::thread([this](){this->io_context.run();});
}

Client::~Client()
{
	this->io_context.stop();
	if(this->thread_context.joinable()) this->thread_context.join();
	if(this->connection)	delete this->connection;
}

}
