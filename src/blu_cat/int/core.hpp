/*
 * Copyright 2022-2024 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef BLU_CAT_INT_CORE_H
#define BLU_CAT_INT_CORE_H 1

#define BLU_CAT_VERSION_MAJOR 0
#define BLU_CAT_VERSION_MINOR 1
#define BLU_CAT_VERSION_PATCH 0

#include <chrono>
#include <cstdint>
#include <memory>
#include <random>

#include "../com/command.hpp"
#include "../com/job_queue.hpp"
#include "../com/worker.hpp"
#include "../gra/device.hpp"
#include "../gra/descriptor_set_layout.hpp"
#include "../gra/framebuffer.hpp"
#include "../gra/graphics_pipeline_2d_solid_layout.hpp"
#include "../gra/graphics_pipeline_2d_wired_layout.hpp"
#include "../gra/graphics_pipeline_2d_solid.hpp"
#include "../gra/graphics_pipeline_2d_wired.hpp"
#include "../gra/graphics_pipeline_3d_layout.hpp"
#include "../gra/graphics_pipeline_3d.hpp"
#include "../gra/graphics_pipeline_3d_skeletal.hpp"
#include "../gra/graphics_pipeline_sprite_3d.hpp"
#include "../gra/light.hpp"
#include "../gra/log.hpp"
#include "../gra/render_pass.hpp"
#include "../gra/renderer.hpp"
#include "../gra/swapchain.hpp"
#include "../gra/vulkan.hpp"

namespace BluCat::INT
{

extern std::random_device random_seed;
extern std::mt19937 random_number_generator;

struct Core
{
  static const CommandChain loader;

  Log::Logger log;

  COM::JobQueue job_queue;
  std::vector<COM::Worker> workers;
  std::vector<std::thread> threads;

  /// Text displayed in the game window.
  std::string game_name;

  /**
   * @{
   * This is the ammount of pixel that the games uses when rendering to the
   * screen.
   */
  uint32_t display_width, display_height;
  /// @}

  int game_version_major, game_version_minor, game_version_patch;

  uint32_t fps;
  std::chrono::duration<long long, std::milli> max_frame_duration;
  float delta_time;

  FT_Library font_library;

  VkSurfaceKHR window_surface;
  VkInstance vk_instance;

#ifdef DEBUG
  VkDebugUtilsMessengerEXT vk_callback;
#endif

  // Vulkan devices.
  std::vector<BluCat::GRA::Device> vk_devices;
  BluCat::GRA::Device *vk_device_with_swapchain;
  BluCat::GRA::Swapchain *vk_swapchain;

  BluCat::GRA::Framebuffer *vk_framebuffer;
  BluCat::GRA::RenderPass *vk_render_pass;
  BluCat::GRA::DescriptorSetLayout *vk_descriptor_set_layout;
  BluCat::GRA::GraphicsPipeline3DLayout *vk_graphics_pipeline_3d_layout;
  BluCat::GRA::GraphicsPipeline2DSolidLayout
	*vk_graphics_pipeline_2d_solid_layout;
  BluCat::GRA::GraphicsPipeline2DWiredLayout
	*vk_graphics_pipeline_2d_wired_layout;
  BluCat::GRA::Light *vk_light;
  std::unique_ptr<BluCat::GRA::GraphicsPipeline3D> vk_graphics_pipeline_3d;
  std::unique_ptr<BluCat::GRA::GraphicsPipeline3DSkeletal>
  vk_graphics_pipeline_3d_skeletal;
  std::unique_ptr<BluCat::GRA::GraphicsPipelineSprite3D>
	vk_graphics_pipeline_sprite_3d;
  std::unique_ptr<BluCat::GRA::GraphicsPipeline2DSolid>
	vk_graphics_pipeline_2d_solid;
  std::unique_ptr<BluCat::GRA::GraphicsPipeline2DWired>
	vk_graphics_pipeline_2d_wired;

  BluCat::GRA::Renderer *vk_renderer;
};

extern Core core;

}

#endif /* BLU_CAT_INT_CORE_H */
