/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "job_queue.hpp"

#include <chrono>

namespace BluCat::COM
{

JobQueue::JobQueue():
  jobs{},
  _stop{false}
{
}

void
JobQueue::stop()
{
  using namespace std::chrono_literals;

  while(!this->jobs.empty()) std::this_thread::sleep_for(1000ms);
  this->_stop = true;
  this->condition.notify_all();
}

void
JobQueue::push(Job job)
{
  std::unique_lock<std::mutex> lock{this->access};
  this->jobs.push_back(job);
  this->condition.notify_one();
}

Job
JobQueue::pop()
{
  std::unique_lock<std::mutex> lock{this->access};
  this->condition.wait(lock, [this]{
    return !this->jobs.empty() || this->_stop;});
  if(!this->jobs.empty())
  {
    auto job{std::move(this->jobs.front())};
    this->jobs.pop_front();
    return job;
  }
  else
  {
    return Job{nullptr};
  }
}

}
