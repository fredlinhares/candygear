/*
 * Copyright 2022-2023 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#version 450
#extension GL_ARB_separate_shader_objects : enable

struct DataTransferObject
{
  vec4 frag_color;
  vec2 frag_texture_coord;
  vec3 normal;
};

layout(location = 0) in DataTransferObject in_dto;

layout(location = 0) out vec4 out_color;

layout(set = 0, binding = 1) uniform UBOWorld
{
  vec3 directional_light_direction;
  vec4 directional_light_color;
} ubo_world;

layout(set = 3, binding = 0) uniform sampler2D texture_sampler;

void
main()
{
  float diffuse_factor =
    max(dot(in_dto.normal, -ubo_world.directional_light_direction), 0.0);

  vec4 diff_samp = texture(texture_sampler, in_dto.frag_texture_coord);
  vec4 ambient = vec4(vec3(in_dto.frag_color), diff_samp.a);
  vec4 diffuse =
    vec4(vec3(ubo_world.directional_light_color * diffuse_factor),
	 diff_samp.a);

  diffuse *= diff_samp;
  ambient *= diff_samp;

  out_color = (ambient + diffuse);
}
