/*
 * Copyright 2022-2023 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 in_texture_coord;

layout(location = 0) out DataTransferObject
{
  vec4 frag_color;
  vec2 frag_texture_coord;
} out_dto;

layout(set = 0, binding = 0) uniform UBOWorld
{
  vec4 ambient_light_color;
} ubo_world;

layout(set = 1, binding = 0) uniform UBOView
{
  mat4 view;
  mat4 proj;
} ubo_view;

layout(set = 2, binding = 0) uniform UBOSprite3D
{
  vec3 position;
  vec2 size;
} ubo_position;

void
main()
{
  vec4 position = vec4(ubo_position.position, 1.0);
  position = ubo_view.view * position;
  switch(gl_VertexIndex)
  {
  case 0:
    position.x -= ubo_position.size.x/2;
    position.y -= ubo_position.size.y/2;
    break;
  case 1:
    position.x -= ubo_position.size.x/2;
    position.y += ubo_position.size.y/2;
    break;
  case 2:
    position.x += ubo_position.size.x/2;
    position.y -= ubo_position.size.y/2;
    break;
  case 3:
    position.x += ubo_position.size.x/2;
    position.y += ubo_position.size.y/2;
    break;
  }
  gl_Position = ubo_view.proj * position;

  out_dto.frag_color = ubo_world.ambient_light_color;
  out_dto.frag_texture_coord = in_texture_coord;
}
