/*
 * Copyright 2022-2023 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#version 450
#extension GL_ARB_separate_shader_objects : enable

#define MAX_NUM_OF_INFLUENCING_BONES 4
#define MAX_NUM_OF_BONES 50

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texture_coord;
layout(location = 3) in ivec4 in_bone_ids;
layout(location = 4) in vec4 in_bone_weights;

layout(location = 0) out DataTransferObject
{
  vec4 frag_color;
  vec2 frag_texture_coord;
  vec3 normal;
} out_dto;

layout(set = 0, binding = 0) uniform UBOWorld
{
  vec4 ambient_light_color;
} ubo_world;

layout(set = 1, binding = 0) uniform UBOView
{
  mat4 view;
  mat4 proj;
} ubo_view;

layout(set = 2, binding = 0) uniform UBOSkeletalModel
{
  mat4 base_matrix;
  mat4 bone_matrices[MAX_NUM_OF_BONES];
} ubo_skeletal_model;

void
main()
{
  vec4 position = vec4(0.0f);
  vec3 normal = in_normal;
	if(in_bone_weights[0] == 0.0)
	{
		position = vec4(in_position, 1.0);
		normal = mat3(ubo_skeletal_model.base_matrix) * in_normal;
	}
	else
	{
		for(int i = 0; i < MAX_NUM_OF_INFLUENCING_BONES; i++)
		{
			if(in_bone_weights[i] == 0.0) break;

			vec4 local_position =
				ubo_skeletal_model.bone_matrices[in_bone_ids[i]] *
				vec4(in_position, 1.0f);
			position += local_position * in_bone_weights[i];
			normal = mat3(ubo_skeletal_model.bone_matrices[in_bone_ids[i]]) * normal;
		}
	}

  mat4 view_model = ubo_view.view * ubo_skeletal_model.base_matrix;
  gl_Position = ubo_view.proj * view_model * position;
  out_dto.frag_color = ubo_world.ambient_light_color;
  out_dto.frag_texture_coord = in_texture_coord;
  out_dto.normal = normal;
}
