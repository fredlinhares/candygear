/*
 * Copyright 2022 Frederico de Oliveira Linhares
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UBOView2D
{
  mat4 proj;
} ubo_view;

layout(push_constant) uniform PCRectangle
{
  vec4 position;
} sprite;

void
main()
{
  vec2 coordinate;

  switch(gl_VertexIndex)
  {
  case 0:
    coordinate = vec2(sprite.position.x, sprite.position.y);
    break;
  case 1:
    coordinate = vec2(sprite.position.x, sprite.position.w);
    break;
  case 2:
    coordinate = vec2(sprite.position.z, sprite.position.y);
    break;
  case 3:
    coordinate = vec2(sprite.position.z, sprite.position.w);
    break;
  }
  gl_Position = ubo_view.proj * vec4(coordinate, 0.0, 1.0);
}
