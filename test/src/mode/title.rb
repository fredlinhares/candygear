# Copyright 2022-2023 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module Mode
  class Title
    def initialize()
      @view = CandyGear::View2D.new(
        CandyGear::Vector4D.new(0, 0, 1280, 720), 640, 360);
      CandyGear.views = [@view];

      @menu = CandyGear::Menu.new(
        @view,
        $global_data[:menu_view], 10, 10,
        [
          {text: "Demo", action: -> {change_mode(:demo);}},
          {text: "Quit", action: -> {CandyGear.quit();}}
        ]);
    end

    def key_down(key)
      case key
      when CandyGear::Key::I
        @menu.pred_opt();
      when CandyGear::Key::K
        @menu.next_opt();
      when CandyGear::Key::F
        @menu.activate();
      end
    end

    def key_up(key)
    end

    def tick()
      @menu.draw();
    end
  end
end
