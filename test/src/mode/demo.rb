# Copyright 2022-2023 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module Mode
  class Demo
    CAMERA_ROTATION_SPEED = Math::PI/45;
    BOX_ROTATION_SPEED = Math::PI/360;
    TRANSLATION_SPEED = 0.5;

    def initialize()
      texture = CandyGear::Texture.from_image("textures/color_texture.qoi");
      mesh = CandyGear::StaticMesh.new("meshes/cube.cgmesh");
      skeletal_mesh = CandyGear::SkeletalMesh.new("meshes/cuboid.cgmesh");
      font = CandyGear::Font.new("/usr/share/fonts/TTF/HanaMinA.ttf", 30);
      japanese_text = CandyGear::Texture.from_text(
        font, "こんにちは世界!");
      english_text = CandyGear::Texture.from_text(
        font, "The quick brown fox jumps");

      @color = CandyGear::Vector3D.new(0.8, 0.2, 0.2);
      @sprite = CandyGear::Sprite.new(texture, 0, 0, 1.0, 1.0);
      @rectangle = CandyGear::Vector4D.new(103.0, 1.0, 100.0, 100.0);
      @sprite_position = CandyGear::Vector4D.new(1.0, 1.0, 100.0, 100.0);
      @japanese_text_sprite = CandyGear::Sprite.new(
        japanese_text, 0, 0, 1.0, 1.0);
      @japanese_text_position = CandyGear::Vector4D.new(
        204.0, 1.0, japanese_text.width, japanese_text.height);
      @english_text_sprite = CandyGear::Sprite.new(
        english_text, 0, 0, 1.0, 1.0);
      @english_text_position = CandyGear::Vector4D.new(
        204.0, japanese_text.height + 2.0,
        english_text.width, english_text.height);

      instance_positions = [
        CandyGear::Vector3D.new(5.0, 0.0, 0.0),
        CandyGear::Vector3D.new(-5.0, 0.0, 0.0),
        CandyGear::Vector3D.new(0.0, 5.0, 0.0),
        CandyGear::Vector3D.new(0.0, -5.0, 0.0),
        CandyGear::Vector3D.new(0.0, 0.0, 5.0),
        CandyGear::Vector3D.new(0.0, 0.0, -5.0)
      ];
      @instances_orientation = CandyGear::Orientation3D.new(0.0, 0.0, 0.0);

      @instances = [
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[0], @instances_orientation),
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[1], @instances_orientation),
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[2], @instances_orientation),
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[3], @instances_orientation),
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[4], @instances_orientation),
        CandyGear::StaticModel.new(
          mesh, texture, instance_positions[5], @instances_orientation)
      ]

      skeletal_mesh_position = CandyGear::Vector3D.new(0.0, 0.0, 0.0);
      skeletal_mesh_orientation = CandyGear::Orientation3D.new(0.0, 0.0, 0.0);
      @skeletal_model = CandyGear::SkeletalModel.new(
        skeletal_mesh, texture, skeletal_mesh_position,
        skeletal_mesh_orientation);

      sprite_3d_position = CandyGear::Vector3D.new(0.0, 0.0, 0.0);
      @sprite_3d = CandyGear::Sprite3D.new(
        @sprite, sprite_3d_position, 1.0, 1.0);

      @camera_position = CandyGear::Vector3D.new(0.0, 0.0, 20.0);
      @camera_orientation = CandyGear::Orientation3D.new(0.0, 0.0, 0.0);

      color = CandyGear::Vector3D.new(0.12, 0.12, 0.18);
      @view1 = CandyGear::View2D.new(
        CandyGear::Vector4D.new(0, 0, 1280, 240), 640, 120);
      @view2 = CandyGear::View3D.new(
        CandyGear::Vector4D.new(0, 240, 1280, 480), 1280, 480);
      CandyGear.views = [@view1, @view2];

      @view2.camera_position = @camera_position;
      @view2.camera_orientation = @camera_orientation;
    end

    def key_down(key)
      case key
      when CandyGear::Key::I
        @camera_orientation.rotate(-CAMERA_ROTATION_SPEED, 0.0, 0.0);
      when CandyGear::Key::K
        @camera_orientation.rotate(CAMERA_ROTATION_SPEED, 0.0, 0.0);
      when CandyGear::Key::J
        @camera_orientation.rotate(0.0, CAMERA_ROTATION_SPEED, 0.0);
      when CandyGear::Key::L
        @camera_orientation.rotate(0.0, -CAMERA_ROTATION_SPEED, 0.0);
      when CandyGear::Key::E
        @camera_position.translate(
          CandyGear::Vector3D.new(
            0.0, 0.0, -TRANSLATION_SPEED), @camera_orientation);
      when CandyGear::Key::D
        @camera_position.translate(
          CandyGear::Vector3D.new(
            0.0, 0.0, TRANSLATION_SPEED), @camera_orientation);
      when CandyGear::Key::S
        @camera_position.translate(
          CandyGear::Vector3D.new(
            -TRANSLATION_SPEED, 0.0, 0.0), @camera_orientation);
      when CandyGear::Key::F
        @camera_position.translate(
          CandyGear::Vector3D.new(
            TRANSLATION_SPEED, 0.0, 0.0), @camera_orientation);
      end
    end

    def key_up(key)
    end

    def tick()
      @sprite.draw(
        @view1, @sprite_position.x, @sprite_position.y,
        @sprite_position.w, @sprite_position.h);
      @japanese_text_sprite.draw(
        @view1, @japanese_text_position.x, @japanese_text_position.y,
        @japanese_text_position.w, @japanese_text_position.h);
      @english_text_sprite.draw(
        @view1, @english_text_position.x, @english_text_position.y,
        @english_text_position.w, @english_text_position.h);
      @instances_orientation.rotate(0.0, BOX_ROTATION_SPEED, 0.0);
      @rectangle.draw_rectangle(@view1, @color);
      @instances.each {_1.draw()};
      @skeletal_model.draw();
      # @sprite_3d.draw();
    end
  end
end
