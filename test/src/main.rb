# Copyright 2022-2023 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

def change_mode(new_mode)
  $next_stage = new_mode;
  $quit_stage = true;
end

def config()
  CandyGear.game_name = "Candy Gear Test";
  CandyGear::Graphic.display_width = 1280;
  CandyGear::Graphic.display_height = 720;
  CandyGear::Graphic.fps = 60;
end

def init()
  menu_texture = CandyGear::Texture.from_image("textures/menu.qoi");
  # FIXME: Text rendering crashes with this font:
  # font = CandyGear::Font.new(
  #   "/usr/share/fonts/TTF/sazanami-mincho.ttf", 16);
  # FIXME: This font, under this path, may not be present in all Linuxes:
  font = CandyGear::Font.new("/usr/share/fonts/TTF/HanaMinA.ttf", 18);
  $global_data = {
    font: font,
    menu_view: CandyGear::Menu::BorderedView.new(menu_texture, font, 16, 16)
  }

  change_mode(:title);
end

def key_down(key) = $mode.key_down(key);

def key_up(key) = $mode.key_up(key);

def quit() = CandyGear.quit();

def tick()
  if $quit_stage then
    case $next_stage
    when :title
      $mode = Mode::Title.new();
    else
      $mode = Mode::Demo.new();
    end
    $quit_stage = false;
  end

  $mode.tick();
end
