# Copyright 2022-2023 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module CandyGear::Animation
  class Discrete
    Step = Struct.new(:duration, :obj);

    def initialize(steps, looping=true)
      @steps = steps.map { Step.new(_1[0], _1[1]); };
      @looping = looping;

      self.reset();
    end

    def tick()
      step = @steps[@index];

      return step.obj if @sequence_over;

      @current_time += 1;

      if @current_time >= step.duration then
        @current_time = 0;
        @index += 1;

        if @index >= @steps.size then
          if @looping then
            @index = 0; # It is looping, go back to first step.
          else
            @index -= 1;
            @sequence_over = true;
          end
        end
      end

      return step.obj;
    end

    def obj() = @steps[@index].obj;

    def reset()
      @index = 0;
      @current_time = 0;
      @sequence_over = false;
    end
  end
end
