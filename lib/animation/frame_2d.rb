# Copyright 2022-2023 Frederico de Oliveira Linhares
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module CandyGear
  module Animation
    class Frame2D
      def initialize(sprite, x, y, width, height)
        @position = Vector4D.new(x, y, width, height);
        @sprite = sprite;
      end

      def draw(view, pivot_x, pivot_y)
        @sprite.draw(
          view,
          @position.x + pivot_x, @position.y + pivot_y,
          @position.w, @position.h);
      end
    end
  end
end
