CandyGear
=========

Candy Gear is a 2D and 3D game engine for mruby.
Its primary goal is to work as a minimalistic facade between the game and the system running it (ex.: Linux, Nintendo Switch, PlayStation, etc.).

Cady Gear aims to be minimalistic yet very flexible.
To achieve this goal, instead of providing a wide range of functionalities with particular uses, it provides a small set of abstract functionalities that do not dictate how to organize your game's code.
However, because of this flexibility, the engine presumes that you have some basic knowledge of algorithms, data structures, and Ruby.

Installation
------------

CandyGear uses the libraries libSDL2, SDL2\_mixer, and freetype, so before installing CandyGear into a machine, ensure that development libraries for SDL are installed.
CandyGear is compiled with Ruby Rake; also ensure it is installed.
To compile the code, run `rake` at the root directory; it will generate the binary engine.
To install the core, run `rake install`.
