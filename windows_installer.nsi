; Copyright 2022-2024 Frederico de Oliveira Linhares
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

!define UNINSTNAME "uninstall"
!define REGAPPKEY "CandyGear"
!define REGHKEY HKLM
!define REGPATH "Software"
!define REGPATH_WINUNINST "Software\Microsoft\Windows\CurrentVersion\Uninstall"

Name "Candy Gear $%CANDY_GEAR_VERSION%"
InstallDir "$PROGRAMFILES64\CandyGear"
OutFile "CandyGear-$%CANDY_GEAR_VERSION%.exe"

Page directory
Page instfiles

UninstPage instfiles

Section
  SetOutPath "$INSTDIR"
  File candy_gear.exe
  File "$%DLL_PATH%\\libbrotlicommon.dll"
  File "$%DLL_PATH%\\libbrotlidec.dll"
  File "$%DLL_PATH%\\libbz2-1.dll"
  File "$%DLL_PATH%\\libfreetype-6.dll"
  File "$%DLL_PATH%\\libgcc_s_seh-1.dll"
  File "$%DLL_PATH%\\libglib-2.0-0.dll"
  File "$%DLL_PATH%\\libgraphite2.dll"
  File "$%DLL_PATH%\\libharfbuzz-0.dll"
  File "$%DLL_PATH%\\libiconv-2.dll"
  File "$%DLL_PATH%\\libintl-8.dll"
  File "$%DLL_PATH%\\libmpg123-0.dll"
  File "$%DLL_PATH%\\libogg-0.dll"
  File "$%DLL_PATH%\\libopus-0.dll"
  File "$%DLL_PATH%\\libopusfile-0.dll"
  File "$%DLL_PATH%\\libpcre2-8-0.dll"
  File "$%DLL_PATH%\\libpng16-16.dll"
  File "$%DLL_PATH%\\libstdc++-6.dll"
  File "$%DLL_PATH%\\libwinpthread-1.dll"
  File "$%DLL_PATH%\\SDL2.dll"
  File "$%DLL_PATH%\\SDL2_mixer.dll"
  File "$%DLL_PATH%\\zlib1.dll"

  SetOutPath "$INSTDIR\glsl"
  File glsl\*.spv

  SetOutPath "$INSTDIR\lib"
  File /r lib\*.rb

  WriteUninstaller $INSTDIR\${UNINSTNAME}.exe

  WriteRegStr ${REGHKEY} "${REGPATH}\${REGAPPKEY}" "InstallLocation" \
    "$INSTDIR"

  WriteRegStr ${REGHKEY} "${REGPATH_WINUNINST}\${REGAPPKEY}" "DisplayName" \
    "CandyGear"
  WriteRegStr ${REGHKEY} "${REGPATH_WINUNINST}\${REGAPPKEY}" \
    "UninstallString" "$INSTDIR\${UNINSTNAME}.exe"
  WriteRegStr ${REGHKEY} "${REGPATH_WINUNINST}\${REGAPPKEY}" \
    "InstallLocation" "$INSTDIR"
SectionEnd

Section "Uninstall"
  RMDir /REBOOTOK /r $INSTDIR\lib

  Delete /REBOOTOK $INSTDIR\glsl\*.spv
  RMDir /REBOOTOK $INSTDIR\glsl

  Delete /REBOOTOK $INSTDIR\*.dll
  Delete /REBOOTOK $INSTDIR\candy_gear.exe
  Delete $INSTDIR\uninstall.exe

  DeleteRegKey ${REGHKEY} "${REGPATH}\${REGAPPKEY}"
  DeleteRegKey ${REGHKEY} "${REGPATH_WINUNINST}\${REGAPPKEY}"
SectionEnd
